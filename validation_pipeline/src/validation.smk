#snakemake version=5.5.4
## Workflow idea:
configfile:"config_models.yaml"
# Config with species and their read dirs, also plascope model and RFPlasmids model

def getrfpModel(wildcards):
    return config["species"][wildcards.species]["rfplasmids"]
def getspeciesname(wildcards):
    return config["species"][wildcards.species]["mlplasmids"]
def getReadsDir(wildcards):
    #print(wildcards)
    return config["species"][wildcards.species]["readsdir"]

from itertools import product
from collections import defaultdict

import glob
import re
import os.path
DEBUG = False  
#DEBUG=True


def getReads(readsdir):
    samples = []
    location = {}
    for file in glob.glob(readsdir + "/**"):
        split = re.split('/|_R[1|2]', file)
        filename, directory = split[-2], split[-3]
        location[filename] = directory
        samples.append(filename)
    if not DEBUG:
        return([samples, location])
    else:
        return([samples[1:2], location])
    
# Aggregate all the sample names per species
data=config["species"]
readsdirs = [data[x]["readsdir"] for x in data]
#print(readsdirs)
samples = []
location = {}
specieslist = list(data.keys())
for r in readsdirs:
    rsample, rlocation = getReads(r)
    #print(rsample, rlocation)
    samples.append(rsample)
    location.update(rlocation)

def samplesNotDone(movable = False):
    #print(specieslist) 
    #print(samples)
    movedict = defaultdict(list)
    speclist = [[sample,spec] for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]    
    for x in speclist:
        files = [f"../results/{x[1]}/07_gplas_predict/{x[0]}_predict_done.txt",
                 f"../results/{x[1]}/11_mobsuite_quast/{x[0]}_mobsuite_quast_done.txt",
                 f"../results/{x[1]}/09_plasmidspades/{x[0]}_binning_done.txt",
               ]        
        undones = [os.path.isfile(y) for y in files]
        if not movable:
            print(f"{x[0]} -- {sum(undones)} done steps")
        if sum(undones) < 3:
            movedict[x[1]].append(x[0])
  
    if movable:
        for x in movedict.keys():
            print(f"Species: {x}")        
            output = "\n".join(movedict[x])
            print(output)
                 


samplesNotDone(movable=True)

rule all:
    input:
        #expand("{species}_done.txt",species=config["species"]),
        expand("../results/{species}/01_lists/{species}_readslist.txt",species=config["species"]),
        #expand("../results/tmp/{species}/{sample}_done.tmp", species=config["species"], sample=samples),
        expand(["../results/{species}/03_reads/fastqc/{sample}_fastqc_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/03_reads/fastqc/{sample}_postfilter_fastqc_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/03_reads/trimmed/{sample}_trimming_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/04_assembly/{sample}_assembly_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/06_plasmidEC/{sample}_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        #expand(["../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_plasmid_prediction.tab".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/07_gplas_predict/{sample}_predict_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        #expand(["../results/{species}/20_gplas_extended/{sample}_extended_predict_b20_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/08_mobsuite/{sample}_mobsuite_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/09_plasmidspades/{sample}_plasmidspades_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/09_plasmidspades/{sample}_binning_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/10_gplas_quast/{sample}_gplas_quast_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        #expand(["../results/{species}/21_gplas_extended_quast/{sample}_gplas_extended_quast_b20_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/11_mobsuite_quast/{sample}_mobsuite_quast_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/12_plasmidspades_quast/{sample}_plasmidspades_quast_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        expand(["../results/{species}/19_assembly_quast/{sample}_assembly_nodes_quast_done.txt".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        "../results/mobsuite_init_done.txt",
        #expand("independent_prediction/{sample}_predict_done.txt",sample=samples),
        #expand(["../results/{species}/{sample}_trimmed_R1.fastq".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        #expand(["../results/{species}/{sample}_trimmed_R2.fastq".format(species=spec, sample=sample) for (zspec,zsample) in zip(specieslist,samples) for (spec,sample) in product([zspec],zsample)]),
        #expand("../results/tmp/{sample}_done.tmp",sample=glob_wildcards(f"{{config["species"][wildcards.species]["readsdir"]}}/{{sample}}_R1.fastq").sample),

rule gatherSamples:
    input:
        getReadsDir
    output:
        "../results/{species}/01_lists/{species}_readslist.txt"
    shell:
        """
        for x in $(ls {input}/*R1.fastq*); do
            basename ${{x}} _R1.fastq >> {output}
        done
        """

# Run QC

rule fastqc:
    input:
        forward=lambda wildcards: expand("{dir}/{sample}_R1.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
        reverse=lambda wildcards: expand("{dir}/{sample}_R2.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
        #"{dir}/{species}/{sample}_R2.fastq".format(dir=getReadsDir, species=location[wildcards.sample], sample=wildcards.sample), 
     
    output:
        "../results/{species}/03_reads/fastqc/{sample}_fastqc_done.txt",
        directory("../results/{species}/03_reads/fastqc/{sample}/")
    threads: 2
    shell:
        """
        mkdir -p {output[1]}
        fastqc {input.forward} {input.reverse} -t 2 --outdir {output[1]} 
        touch {output[0]}
        """    

rule trimgalore:
    input:
        forward=lambda wildcards: expand("{dir}/{sample}_R1.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
        reverse=lambda wildcards: expand("{dir}/{sample}_R2.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
    output:
        directory("../results/{species}/03_reads/trimmed/{sample}/"),
        "../results/{species}/03_reads/trimmed/{sample}_trimming_done.txt"
    threads: 8
    resources:
        runtime=90
    shell:
        """
        trim_galore --quality 20 --gzip --length 20 --paired -j {threads} --output_dir {output[0]} {input.forward} {input.reverse}
        touch {output[1]}
        """


# Run Unicycler on reads
'''
rule assembly:
    input:
        #forward=lambda wildcards: expand("{dir}_{sample}_R1.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
        #reverse=lambda wildcards: expand("{dir}_{sample}_R2.fastq".format(dir=getReadsDir(wildcards), species=wildcards.species, sample=wildcards.sample)),
        #timeline=lambda wildcards: expand("../results/{species}/03_reads/trimmed/{sample}_trimming_done.txt".format(species=wildcards.species, sample=wildcards.sample)),
        ancient("../results/{species}/03_reads/trimmed/{sample}_trimming_done.txt")
    output:
        #directory("../results/{species}/04_assembly/{sample}/"),
        "../results/{species}/04_assembly/{sample}_assembly_done.txt"
    shell:
        """ 
        touch {output[1]}
        """
'''
rule assembly:
    input:
        directory("../results/{species}/03_reads/trimmed/{sample}/")
    output:
        directory("../results/{species}/04_assembly/{sample}/"),
        "../results/{species}/04_assembly/{sample}_assembly_done.txt"
    threads: 32
    resources:
        runtime=600,
        mem=100
    shell:
        """
        unicycler -1 {input}/*_R1_val_1.fq.gz -2 {input}/*_R2_val_2.fq.gz -o {output[0]} --threads {threads} --min_fasta_length 1000
        touch {output[1]}
        """

rule fastqc_postfilter:
    input:
        directory("../results/{species}/03_reads/trimmed/{sample}/"),
        timeline="../results/{species}/03_reads/trimmed/{sample}_trimming_done.txt"
        #"{dir}/{species}/{sample}_R2.fastq".format(dir=getReadsDir, species=location[wildcards.sample], sample=wildcards.sample),
    output:
        "../results/{species}/03_reads/fastqc/{sample}_postfilter_fastqc_done.txt",
        directory("../results/{species}/03_reads/fastqc/{sample}_postfilter/")
    threads: 2
    shell:
        """
        mkdir -p {output[1]}
	fastqc {input[0]}/*_R1_val_1.fq.gz {input[0]}/*_R2_val_2.fq.gz -t 2 --outdir {output[1]}
        touch {output[0]}
        """

# Run PlasmidEC
rule plasmidEC:
    input:
        ancient("../results/{species}/04_assembly/{sample}_assembly_done.txt"),
        #"../results/{species}/02_models/"
    output:
        directory("../results/{species}/06_plasmidEC/{sample}"),
        #"../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_raw_nodes_plasmid_prediction.tab",        
        "../results/{species}/06_plasmidEC/{sample}_done.txt"
    resources:
        runtime=360
    params:
        rfp= lambda wildcards: getrfpModel(wildcards),
        sample="{sample}",
        #speciesname=lambda wildcards: getspeciesname(wildcards),
        speciesname="{species}",
        graph="../results/{species}/04_assembly/{sample}/assembly.gfa"
    threads: 16
    shell:
        """
        #bash plasmidEC-1/plasmidEC.sh -i {input[0]} -o {output[0]}  -f -g -s "{params.speciesname}"
        #bash plasmidEC-1/plasmidEC.sh -i {input[0]} -o {output[0]} -c plascope,rfplasmids,platon -p ../results/{wildcards.species}/02_models -d {wildcards.species}_plasmid_db -r {params.rfp} -g
        bash plasmidEC/plasmidEC.sh -i {params.graph} -o {output[0]} -c centrifuge,rfplasmids,platon -p ../results/{wildcards.species}/02_models -d {wildcards.species}_plasmid_db -r {params.rfp} -g 
        mv {output[0]}/gplas_format/assembly_plasmid_prediction.tab {output[0]}/gplas_format/{params.sample}_plasmid_prediction.tab
        touch {output[1]}
        """

# Run gplas DEPRECATED BY NEW PLASMIDEC RELEASE
#rule gplas_extract:
#    input:
#        "../results/{species}/04_assembly/{sample}/",
#        "../results/{species}/04_assembly/{sample}_assembly_done.txt",
#    output:
#        "../results/{species}/05_gplas/{sample}/{sample}_raw_nodes.fasta",
#        "../results/{species}/05_gplas/{sample}_extraction_done.txt",
#    params:
#        sample="{sample}",
#        outdir="../results/{species}/05_gplas/{sample}/"
#    shell:
#        """
#        python -m gplas.gplas -i {input[0]}/assembly.gfa -c extract -n {params.sample}
#        mkdir -p {params.outdir}
#        cp gplas_input/{params.sample}_raw_nodes.fasta {output[0]}
#        touch {output[1]}
#        """

#rule plasmidEC_rename:
#    input:
#        "../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_raw_nodes_plasmid_prediction.tab"
#    output:
#        "../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_plasmid_prediction.tab"
##    shell:
#        """
#        #grep -v "NA" {input[0]} > {output[0]}
#        mv {input[0]} {output[0]}
#        """


#rule gplas_copy:
#    input:
#        "../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_plasmid_prediction.tab"
#        "../results/{species}/06_plasmidEC/{sample}_done.txt",
#    output:
#        "independent_prediction/{sample}_plasmid_prediction.tab",
#    params:
#        sample="{sample}"
#    shell:
#        """
#        mkdir -p {output[0]}
#        cp {input[1]} {output[0]}
#        """

rule gplas_predict:
    input:
        "../results/{species}/06_plasmidEC/{sample}_done.txt",
        "../results/{species}/04_assembly/{sample}/",
        ancient("../results/{species}/04_assembly/{sample}_assembly_done.txt")
    output:
        "../results/{species}/07_gplas_predict/{sample}_predict_done.txt",
        directory("../results/{species}/07_gplas_predict/{sample}_results/")
    params:
        indir="../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_plasmid_prediction.tab",
        sample="{sample}"
    log:
        err="../results/{species}/logs/{sample}_gplas_predict_log.txt"
    shell:
        """
        python -m gplas.gplas -i {input[1]}/assembly.gfa -c predict -n {params.sample} --keep -P {params.indir} -x 50 2> {log.err}
        mkdir -p {output[1]}
        mkdir -p {output[1]}/walks_b5
        mkdir -p {output[1]}/coverage_b5
        mv results/{params.sample}* {output[1]}
        #mv walks/{params.sample}* {output[1]}/walks_b5
        #mv coverage/{params.sample}* {output[1]}/coverage_b5
        touch {output[0]}
        """            

rule gplas_predict_extended:
    input:
        "../results/{species}/06_plasmidEC/{sample}_done.txt",
        "../results/{species}/04_assembly/{sample}/",
        ancient("../results/{species}/04_assembly/{sample}_assembly_done.txt")
    output:
        "../results/{species}/20_gplas_extended/{sample}_extended_predict_b20_done.txt",
        directory("../results/{species}/20_gplas_extended/{sample}_results_b20/")
    params:
        indir="../results/{species}/06_plasmidEC/{sample}/gplas_format/{sample}_plasmid_prediction.tab",
        sample="{sample}",
        cycles=" 50"
    log:
        err="../results/{species}/logs/{sample}_gplas_extended_log.txt"
    shell:
        """
        # RULE CHANGED TO --keep run
        python -m gplas.gplas -i {input[1]}/assembly.gfa -c predict -n {params.sample} -P {params.indir} --keep -x {params.cycles} -b 20 -k 2> {log.err}
        mkdir -p {output[1]}/walks/
        mkdir -p {output[1]}/coverage/
        mv results/{params.sample}* {output[1]}
        #mv walks/*/{params.sample}* {output[1]}/walks/
        #mv walks/{params.sample}* {output[1]}/walks/
        #mv coverage/{params.sample}* {output[1]}/coverage/
        touch {output[0]}
        """            

rule mobsuite_init:
    output: "../results/mobsuite_init_done.txt"
    shell:
        """
        mob_init
        touch {output}
        """

# Run MobSuite
rule mobsuite:
    input:
    	"../results/{species}/06_plasmidEC/{sample}_done.txt",
        "../results/mobsuite_init_done.txt"
    output:
        directory("../results/{species}/08_mobsuite/{sample}/"),
        "../results/{species}/08_mobsuite/{sample}_mobsuite_done.txt"
    params:
        "../results/{species}/06_plasmidEC/{sample}/assembly.fasta",
    shell:
        """
        mob_recon --infile {params[0]} --outdir {output[0]} --force
        touch {output[1]}
        """

# Run plasmidspades

rule plasmidspades:
    input:
        "../results/{species}/03_reads/trimmed/{sample}/"
    output:
        directory("../results/{species}/09_plasmidspades/{sample}/"),
        "../results/{species}/09_plasmidspades/{sample}_plasmidspades_done.txt"
    threads: 16
    resources:
        runtime=360,
        mem=100
    shell:
        """
        spades.py --plasmid -1 {input[0]}*_R1_val_1.fq.gz -2 {input[0]}*_R2_val_2.fq.gz --threads {threads} -o {output[0]}
        touch {output[1]}
        """

# Crunch numbers (new smk file?)

rule quast_gplas:
    input:
        directory("../results/{species}/07_gplas_predict/{sample}_results/"),
        "../results/{species}/07_gplas_predict/{sample}_predict_done.txt"
    output:
        directory("../results/{species}/10_gplas_quast/{sample}/"),
        "../results/{species}/10_gplas_quast/{sample}_gplas_quast_done.txt"
    params:
        sample="{sample}", 
        species="{species}",
        refdir="../data/reference_genomes/"
    shell:
        """
        #run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
        all_bins=$(ls {input[0]}{params.sample}_bin_*.fasta | sed "s#${input[0]}/##g")
        shortsample="$(echo "{params.sample}" | cut -f 1,2 -d '_')"
        for bin in $all_bins
        do
            binname=$(basename ${{bin}} .fasta)
            #quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
            quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
        done
        touch {output[1]}
        """

rule quast_gplas_extended:
    input:
        directory("../results/{species}/20_gplas_extended/{sample}_results_b20/"),
        "../results/{species}/20_gplas_extended/{sample}_extended_predict_b20_done.txt"
    output:
        directory("../results/{species}/21_gplas_extended_quast/{sample}/b20/"),
        "../results/{species}/21_gplas_extended_quast/{sample}_gplas_extended_quast_b20_done.txt"
    params:
        sample="{sample}", 
        species="{species}",
        refdir="../data/reference_genomes/"
    shell:
        """
        #run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
        all_bins=$(ls {input[0]}{params.sample}_bin_*.fasta | sed "s#${input[0]}/##g")
        shortsample="$(echo "{params.sample}" | cut -f 1,2 -d '_')"
        for bin in $all_bins
        do
            binname=$(basename ${{bin}} .fasta)
            #quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
            quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
        done
        touch {output[1]}
        """

rule quast_mobsuite:
    input:
        "../results/{species}/08_mobsuite/{sample}/",
        "../results/{species}/08_mobsuite/{sample}_mobsuite_done.txt",
    output:
        directory("../results/{species}/11_mobsuite_quast/{sample}"),
        "../results/{species}/11_mobsuite_quast/{sample}_mobsuite_quast_done.txt"
    params:
        sample="{sample}", 
        species="{species}",
        refdir="../data/reference_genomes/"
    shell:
        """
        all_bins=$(ls {input[0]}/plasmid*.fasta | sed "s#{input[0]}/{params.sample}/##g")
        shortsample="$(echo "{params.sample}" | cut -f 1,2 -d '_')"        
        for bin in $all_bins
        do
            echo ${{bin}}
            binname=$(basename ${{bin}} .fasta)
            #quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
            quast -o {output[0]}/${{binname}} -r {params.refdir}/{params.species}/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}

        done
        touch {output[1]}
        """

rule split_plasmidspades:
    input:
        "../results/{species}/09_plasmidspades/{sample}/",
        "../results/{species}/09_plasmidspades/{sample}_plasmidspades_done.txt"
    output:
        #directory("../results/{species}/09_plasmidspades/{sample}/bins/"),
        "../results/{species}/09_plasmidspades/{sample}_binning_done.txt"
    params:
        sample="{sample}",
        outdir = "../results/{species}/09_plasmidspades/{sample}/",

    shell:
        """
        echo "GO" >>log.tmp
        if grep -q ">" {input[0]}contigs.fasta ; then
        echo "Nothing wrong"
        else
        echo "No plasmids were found, this will be logged"
        echo "{params.sample}" >> plasmidspades_discards.tmp
        fi

        # Take the contig headers, take the last column (with rev and cut) and take the highest (tail) number
        numcomp=$(cat {input[0]}contigs.fasta | grep component | rev | cut -f 1 -d '_' | sort | tail -1)
        echo ${{numcomp}}
        for component in $(seq -f "component_%g" 0 ${{numcomp}}); do
            echo "${{component}}"
            numlines=$(grep "${{component}}" {input[0]}contigs.fasta | wc -l)
            if [ ${{numlines}} -gt 0 ]; then
            grep "${{component}}" {input[0]}contigs.fasta | sed 's/>//g' > {params.outdir}bin_${{component}}_list.txt
            echo "List complete"
            #grep "${{component}}" {input[0]}contigs.fasta
            faSomeRecords {input[0]}contigs.fasta {params.outdir}bin_${{component}}_list.txt {params.outdir}bin_${{component}}.fasta
            fi
            echo "Extraction complete"
        done
        touch {output[0]}
        """
rule quast_self:
    input:
        "../results/{species}/06_plasmidEC/{sample}_done.txt"
    output:
        directory("../results/{species}/19_assembly_quast/{sample}/"),
        "../results/{species}/19_assembly_quast/{sample}_assembly_nodes_quast_done.txt",
    params:
        sample="{sample}",
        species="{species}",
        refdir="../data/reference_genomes/"
    shell:
        """
        shortsample="$(echo "{params.sample}" | cut -f 1,2 -d '_')"        
        quast -o {output[0]} -r {params.refdir}/{params.species}/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ../results/{params.species}/06_plasmidEC/{params.sample}/assembly.fasta
        #quast -o {output[0]} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ../results/{params.species}/06_plasmidEC/{params.sample}/assembly.fasta
        #quast -o {output[0]} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f*
         
      
        touch {output[1]}
        """
rule quast_plasmidspades:
    input:
        "../results/{species}/09_plasmidspades/{sample}/",
        "../results/{species}/09_plasmidspades/{sample}_binning_done.txt",
    output:
        directory("../results/{species}/12_plasmidspades_quast/{sample}/"),
        "../results/{species}/12_plasmidspades_quast/{sample}_plasmidspades_quast_done.txt"
    params:
        sample="{sample}",
	species="{species}",
	refdir="../data/reference_genomes/"
    shell:
        """
        all_bins=$(ls {input[0]}bin*.fasta | sed "s#{input[0]}{params.sample}/##g")
        shortsample="$(echo "{params.sample}" | cut -f 1,2 -d '_')"
        for bin in $all_bins
        do
            echo ${{bin}}
            binname=$(basename ${{bin}} .fasta)
            #quast -o {output[0]}${{binname}} -r {params.refdir}/{params.species}/refseq/bacteria/"${{shortsample}}"/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}
            quast -o {output[0]}${{binname}} -r {params.refdir}/{params.species}/{params.sample}_genomic.f* -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${{bin}}

        done
	touch {output[1]}
        """




