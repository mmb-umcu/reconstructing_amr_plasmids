#!/bin/bash

#1. get command line optoins
while getopts :r:b:s:g:m:o: flag; do
        case $flag in
		r) complete_references=$OPTARG;; #full path to location of complete reference genomes
                b) benchmark_file=$OPTARG;; # full path to list of benchmarking samples
                s) spades_predictions=$OPTARG;; #full path to directory that contains of all plasmidspades predictions subdirectories (example:/hpc/jesse/results/09_spades_output)
                g) gplas_predictions=$OPTARG;; #as above, but for gplas
		m) mob_predictions=$OPTARG;; #as above, but for MOB-suite
        	o) output_directory=$OPTARG;; #directory for holding output
        esac
done

mkdir -p $output_directory

#source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
#conda activate abricate_mmbioit

#Run abricate for gplas
cd $gplas_predictions
#get the paths to the bin
find $(pwd) -type f | grep .fasta > $output_directory/gplas_paths.txt
abricate --fofn $output_directory/gplas_paths.txt --db resfinder --threads 16 > $output_directory/gplas_abricate_output.csv 

#Run abricate for spades
cd $spades_predictions
#get paths to the bins
find $(pwd) -type f | grep component | grep fasta > $output_directory/spades_paths.txt
abricate --fofn $output_directory/spades_paths.txt  --db resfinder --threads 16 > $output_directory/spades_abricate_output.csv

##Run abricate for mobsuite predictions
cd $mob_predictions
##get paths to the bins
find $(pwd) -type f | grep plasmid | grep fasta > $output_directory/mob_paths.txt
abricate --fofn $output_directory/mob_paths.txt  --db resfinder --threads 16 > $output_directory/mob_abricate_output.csv

#Run abricate for references
cd $complete_references
#get paths to the bins
echo "Running reference sequences"
echo ${benchmark_file}
cat ${benchmark_file} | sed 's/"//g' > ${benchmark_file}.tmp
echo ${benchmark_file}
head ${benchmark_file} | sed 's/"//g'
find $(pwd) -type f | grep -E 'fna|fasta' > $output_directory/reference_genomes_paths.txt
grep -f ${benchmark_file}.tmp ${output_directory}/reference_genomes_paths.txt > ${output_directory}/reference_genomes_benchmark_paths.txt
abricate --fofn $output_directory/reference_genomes_benchmark_paths.txt --db resfinder  --threads 16 > $output_directory/abricate_output_reference_genomes.csv 

