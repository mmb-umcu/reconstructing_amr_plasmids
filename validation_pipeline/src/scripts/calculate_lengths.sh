#!/bin/bash

# Calculates the length of each contig in the reference genomes

# usage: bash calculate_lengths.sh path_to_references speciesname outfile.csv

refpath="$1"
species="$2"
outdir="$3"

plasmids=0
for file in $(ls "${refpath}" | grep "GCF");  do
    echo "${file}"

    reffile="${refpath}"/"${file}"/"${file}" # add *_genomic.fna when using
    if [ -f ${reffile}*_genomic.fna.gz ]; then
        echo "Unzipping file"
        gunzip ${reffile}*_genomic.fna.gz
    fi
    fullname=$(basename $(ls "${reffile}"*_genomic.fna) _genomic.fna)

    # Metafile 1: all plasmids per strain
    for seq in $( grep "plasmid" "${reffile}"*_genomic.fna | cut -f 1 -d ' ' | sed 's/>//g'); do
        echo ${fullname},${seq} >> "${outdir}"/strains_plasmids.csv
    done

    # Logfile for sequence names
    grep ">" ${reffile}*_genomic.fna >> "${outdir}"/${species}_seqlog.txt
    # Metafile 2: get the lengths of all sequences 
    seqkit fx2tab --length --name --only-id "${refpath}"/"${file}"/"${file}"*genomic.fna | sed 's/\t/,/g' >  "${outdir}"/holder.tmp
    # Sum over all the sequences per sample
    #total=$(awk -F ',' '{sum+=$2;} END{print sum;}' "${outdir}"/holder.tmp)
    #echo "${fullname}" "${reffile}"
    # Add to metafile 2: plasmid lengths
    cat "${outdir}"/holder.tmp >> "${outdir}"/benchmarking_lengths.tmp
    cut -f 2 "${outdir}"/strains_plasmids.csv  -d , | grep -f - "${outdir}"/benchmarking_lengths.tmp >> "${outdir}"/benchmarking_plasmids_lenght.csv
    # Metafile 3: strain names
    echo \"${fullname}\" >> ${outdir}/benchmark_strains.csv

done
