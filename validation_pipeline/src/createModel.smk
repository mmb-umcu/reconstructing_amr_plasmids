#/home/dla_mm/jkerkvliet/data/gplas/newmodels/data/genomes_plascope_db
## Workflow idea
configfile: "config_models.yaml"
# config: list of species and their genome dirs
# input: lambda wildcards: expand("{techname}_{capDesign}_{sizeFrac}.readlength.tsv", filtered_product, techname=wildcards.techname, capDesign=wildcards.capDesign, sizeFrac=SIZEFRACS)

def getGenomedir(wildcards):
    return config["species"][wildcards.species]["genomedir"]

rule all:
    input:
        expand("{species}_done.txt",species=config["species"]),
        expand("../results/{species}/00_fasta/{species}_genomes.fasta",species=config["species"]),
        expand("../results/{species}/01_lists/{species}_headers.txt",species=config["species"]),
        expand("../results/{species}/01_lists/{species}_origin.txt",species=config["species"]),

rule createDBFasta:
    input:
        getGenomedir
    output:
        fasta="../results/{species}/00_fasta/{species}_genomes.fasta",
        list="../results/{species}/01_lists/{species}_headers.txt",
        counts="../results/{species}/01_lists/{species}_seqcounts.txt",
        map="../results/{species}/01_lists/{species}_seqmap.txt"
        #done="{species}_done.txt"
    params:
        species="{species}"
    shell:
        """
        mkdir -p ../results/{params.species}/00_fasta
        mkdir -p ../results/{params.species}/01_lists
        
        grep ">" {input[0]}/*.fna | sed 's/:/\t/g' > {output.map}
        grep -c ">" {input[0]}/*.fna | sed 's/:/\t/g' > {output.counts}
        cat {input[0]}/*.fna > {output.fasta}
        grep ">" {output.fasta} | sed 's/>//g' > {output.list}
        """

rule determineOrigin:
    input:
        "../results/{species}/01_lists/{species}_headers.txt",
        "../results/{species}/01_lists/{species}_seqcounts.txt",
        "../results/{species}/01_lists/{species}_seqmap.txt"
    output:
        "../results/{species}/01_lists/{species}_origin.txt",
        "../results/{species}/01_lists/{species}_discard_candidates.txt"
    log:
        "{species}_ambiguous.log"
    run:
        seqmap = open(input[2],'r').readlines()
        seqmap = [[x.split("\t")] for x in seqmap] 
        headers = open(input[0],'r').readlines()
        headers = [x.strip("\n") for x in headers]
        outfile = open(output[0],'w')
        logfile = open(log[0],'w')
        discfile = open(output[1],'w')
        
        for x in headers:
            seqid = x.split(" ")[0] # Get the seqID
            if x.find("plasmid") != -1 or x.find("extrachromosomal") != -1: 
                outfile.write("\t".join([seqid,"3"]))
            elif x.find("chromosome") != -1 or x.find("full genome") != -1:
                outfile.write("\t".join([seqid,"2"]))
                logfile.write("\t".join([x,"chromosome"]))
            elif x.find("chromosome") == -1 and x.find("plasmid") == -1 and x.find("full sequence") != -1:
                logfile.write("\t".join([x,"putative Chromosome"]))
                outfile.write("\t".join([seqid,"2"]))
            else:
                logfile.write("\t".join([x,"ambiguous"]))
                discfile.write("\t".join([x]))
                discfile.write("\n")
            logfile.write("\n")
            outfile.write("\n")
        outfile.close()
        

rule discard_candidates:
    input:
        "../results/{species}/01_lists/{species}_discard_candidates.txt",
        "../results/{species}/00_fasta/{species}_genomes.fasta"
    output:
        "../results/{species}/00_fasta/{species}_clean_genomes.fasta"
    shell:
        """
        faSomeRecords {input[1]} {input[0]} {output[0]} -exclude
        """
        
rule make_db:
    input:
        map="../results/{species}/01_lists/{species}_origin.txt",
        fasta="../results/{species}/00_fasta/{species}_clean_genomes.fasta",
        nodes="../data/plascope_files/nodes.dmp",
        names="../data/plascope_files/names.dmp"
    output:
        "{species}_done.txt",
        directory("../results/{species}/02_models/")
    conda:
        "envs/plascope.yaml"
    params:
        species="{species}"
    threads: 32
    resources:
        runtime=3000,
        mem=500
    shell:
        """
        centrifuge-build -p 32 --conversion-table {input.map} --taxonomy-tree {input.nodes} --name-table {input.names} {input.fasta} {params.species}_plasmid_db
        touch {output[0]}
        mkdir -p {output[1]}
        mv {params.species}_plasmid_db*.cf {output[1]}
        """
        


