# Functions used in the metrics rmd
debugging <- function(){
setwd(dirname(rstudioapi::getSourceEditorContext()$path))
metadir <- metadata_directory
argdir <- '../data/S_enterica/99_final_results_old/05_abricate'
filesep='\t'
benchmark_plasmids <- read_metadata(metadir)
species <- "S. enterica"
quastdir <- "../data/S_enterica/99_final_results/01_quast_self"


testmatrix <- matrix(c("P1","B4",0.66,1,"P2","B1",0.33,0.5,"P2","B3",0.33,0.5,"P3","B1",0.66,0.25,"P3","B2",1,0.25,"P3","B3",0.66,0.25,"P3","B4",0.33,0.25),ncol=4,byrow=T)
testmatrix <- matrix(c(0,0,0,100,25,0,50,0,50,50,50,50),byrow=T,ncol=4) %>% as.data.frame()
testmatrix <- testmatrix %>%
  pivot_longer(c(V1,V2,V3,V4),names_to = "bin_name",values_to = "true_alignment_length") %>%
  mutate(reference_id = c(rep("P1",4),rep("P2",4),rep("P3",4)))

classifier_output <- testmatrix %>% as.data.frame() %>%
  group_by(V2) %>%
  mutate(max_acc = max(V3)) %>%
  filter(V3==max_acc)

}


colornames <- c("gplas2","MOB-suite","plasmidSPAdes","gplas2_plasmidCC")
gplas_palette <-  c('#00FF93','#FFC300','#F11212',"#4285F4")
names(gplas_palette) <- colornames

metadir = "../data/S_aureus/99_final_results/06_lengths/"
read_metadata <- function(metadir){
  wanted_files <- c("strains_plasmids.csv","benchmark_strains.csv","benchmarking_plasmids_length.csv")
  strains_plasmids<-read.csv(file.path(metadir,wanted_files[1]), sep=',', header=FALSE)
  names(strains_plasmids)<-c('strain','reference_id')
  strains_plasmids$classification<-'plasmid'
  
  benchmark_strains<-unique(read.csv(file.path(metadir,wanted_files[2]), sep=',', header=FALSE))
  names(benchmark_strains)<-c('strain')
  benchmark_plasmids<-inner_join(benchmark_strains,strains_plasmids,by='strain') 

  plasmid_lengths<-unique(read.csv(file.path(metadir,wanted_files[3]), sep=',', header=FALSE))
  names(plasmid_lengths)<-c('reference_id','true_plasmid_length')
  plasmid_lengths$reference_id<-gsub(plasmid_lengths$reference_id,pattern = '>',replacement = '')
  plasmid_lengths<-separate(plasmid_lengths,'reference_id',c('reference_id','else'),sep =' ')
  plasmid_lengths<-plasmid_lengths[,c(1,3)]
  benchmark_plasmids<-inner_join(benchmark_plasmids,plasmid_lengths,by='reference_id')
  
  return(benchmark_plasmids)
}

extract_arg_plasmids <- function(benchmark_plasmids, argdir,filesep="\t"){
  # Read abricate output
  abricate_reference_plasmids<-read.csv(file.path(argdir,"abricate_output_reference_genomes.csv"), sep=filesep, header=TRUE) %>%
    distinct()
  
  # Count per replicon
  arg_count_per_replicon<- abricate_reference_plasmids %>% 
    group_by(SEQUENCE) %>% 
    summarise(true_nr_args=n())
  
  names(arg_count_per_replicon)<-c('reference_id','true_nr_args')
  
  # Merge with benchmarking strains
  arg_count_per_plasmid<-inner_join(arg_count_per_replicon,benchmark_plasmids,by=c('reference_id')) %>%
    mutate(strain = sub("_[^_]*$","",strain)) %>% inner_join(benchmark_plasmids[,c(2,3)],by='reference_id')
  
  
  
  benchmark_plasmids_ARG <- benchmark_plasmids %>% 
    left_join(arg_count_per_replicon,by='reference_id') %>%
    mutate(true_nr_args= ifelse(is.na(true_nr_args),0,true_nr_args)) %>%
    mutate(strain = sub("\\.\\d+.*$", "",strain)) %>%
    mutate(arg_plasmid = ifelse(true_nr_args >=1,"ARG-plasmid","non-ARG-plasmid"))
  
  return(benchmark_plasmids_ARG)
  # MONITORING NECESSITY
  #benchmark_plasmids <- benchmark_plasmids %>% mutate(strain = sub("_[^_]*$","",strain))
}  
plot_length_distribution <- function(benchmark_plasmids,species,outfile, save=F){
    threshold <- switch(
      species,
      'E. coli' = 4.25,
      'E. faecalis' = 4.25,
      'E. faecium' = 4.25,
      'K. pneumoniae' = 4.25,
      'S. enterica' = 4.25,
      'S. aureus' = 3.9,
      'A. baumannii' = 4.5,
      'General' = 4.3
    )
  
    length_distribution_plot <- benchmark_plasmids %>% 
      ggplot(aes(x = log(true_plasmid_length, 10))) +
      geom_histogram(aes(y = ..density..), binwidth = 0.05, colour = "black", fill = "#b5b5b5") +
      geom_density(alpha = 0.5, fill = "#FF9933") +
      theme_minimal() + 
      ggtitle('Plasmids length distribution') +
      geom_vline(xintercept = threshold, linetype = 'dashed') +
      theme(
        plot.title = element_text(size = 0, hjust = 0.5, face = 'bold'),
        axis.title.x = element_text(size = 20, face = 'bold'),
        axis.text.x = element_text(size = 18),
        panel.grid.major.x = element_blank(),
        axis.title.y = element_text(size = 24, face = "bold"),
        axis.text.y = element_text(size = 18, angle = 0),
        legend.title = element_text(size = 22),
        legend.text = element_text(size = 20)
      ) +
      xlab('log (length)') + ylab('Density')
    length_distribution_plot
    if(save){
      ggsave(outfile, plot = length_distribution_plot,
             device = 'jpg',
             scale = 1,
             width = 11,
             height = 6,
             units = c("in"),
             dpi = 300,
             limitsize = TRUE)
    }
    return(length_distribution_plot)
}

get_contigmap <- function(quastdir){
  #self_alignment_stats_path<-paste(self_alignment_directory,'alignment_statistics.csv',sep='/')
  quast_self_gfa_output<-read.csv(file.path(quastdir,'alignment_statistics.csv'), sep=',', header=FALSE)  %>%
    select(-V2) # Monitor consistency!!

  names(quast_self_gfa_output)<-c('strain','reference_id','self_alignment_length','reference_contig_count')
  quast_self_gfa_output <- quast_self_gfa_output %>% mutate(strain = sub("\\.\\d+.*$", "",strain)) # not needed for efm
  
  # Overlapping sections
  if(file.exists(file.path(quastdir,'overlapping_length.csv'))){
    quast_self_gfa_overlapping<-read.csv(file.path(quastdir,'overlapping_length.csv'), sep=',', header=FALSE)
    quast_self_gfa_overlapping<-quast_self_gfa_overlapping[,c(1,3,4)]
    names(quast_self_gfa_overlapping)<-c('strain','reference_id','overlapping_length') 
    

    quast_self_gfa_output<- quast_self_gfa_output %>%
      left_join(quast_self_gfa_overlapping,by=c('strain','reference_id')) %>%
      mutate(overlapping_length = ifelse(is.na(overlapping_length),0,overlapping_length)) %>%
      mutate(self_alignment_length = self_alignment_length-overlapping_length) %>%
      select(-overlapping_length)
    
    #quast_self_gfa_output<-quast_self_gfa_output[,-c(5)]
  }
  return(quast_self_gfa_output)
}

calculate_metrics <- function(classifier_output,toolname,long=FALSE){
  
  
  #classifier_output <- classifier_output %>% 
  #  mutate(true_alignment_length = as.numeric(corrected_alignment_length - ambiguous_length))
  
  classifier_output_raw <- classifier_output %>% 
    filter(classification == "plasmid") %>%
    mutate(completeness = round(true_alignment_length/true_plasmid_length,3)) %>%
    #mutate(completeness = round(true_alignment_length/self_alignment_length,3)) %>%
    mutate(accuracy = round(true_alignment_length/bin_length,3)) %>%
    mutate(F1_score = round(2*((accuracy*completeness)/(accuracy+completeness)),3))
  
  classifier_output_maxC <- classifier_output_raw %>%
    group_by(reference_id) %>%
    mutate(repr_plasmid = ifelse(completeness==max(completeness),TRUE,FALSE)) %>%
    group_by(bin_name) %>% 
    mutate(repr_bin = ifelse(accuracy==max(accuracy),TRUE,FALSE))
    #filter(completeness==max(completeness))
  
  if(long){
    metrics_output <- classifier_output_maxC %>% pivot_longer(cols=c(accuracy,completeness,F1_score),names_to="metric")
    
  }else{
    metrics_output <- classifier_output_maxC
  }
  metrics_output$software <- toolname
  return(metrics_output)
}


read_tool_old <- function(tooldir,toolname,benchmark_plasmids,unbinned=FALSE){
  #gplas_alignment_stats_path<-paste(gplas_output_directory,'alignment_statistics.csv',sep='/')
  quast_output_raw<-read.csv(file.path(tooldir,"alignment_statistics.csv"), sep=',', header=FALSE)
  names(quast_output_raw)<-c('strain','bin_name','reference_id','alignment_length','bin_length')
  quast_output_raw <- quast_output_raw %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
  #return(quast_output_raw)
  tool_plasmid_data <- benchmark_plasmids %>% 
    right_join(quast_output_raw,by=c('strain','reference_id')) %>%
    mutate(classification = ifelse(reference_id == 'no_correct_alignments','not_applicable',classification)) %>%
    mutate(classification = replace_na(classification,"chromosome")) %>%
    distinct()
  

  
  #gplas_overlapping_path<-paste(gplas_output_directory,'overlapping_length.csv',sep='/')
  overlapping_path <- file.path(tooldir,'overlapping_length.csv')
  
  if(file.exists(overlapping_path)){
    overlapping <-read.csv(overlapping_path, sep=',', header=FALSE)
    names(overlapping)<-c('strain','bin_name','reference_id','overlapping_length')
    overlapping <- overlapping %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
    tool_plasmid_data <-left_join(tool_plasmid_data,overlapping,by=c('strain','bin_name','reference_id')) %>%
      mutate(overlapping_length = replace_na(overlapping_length,0)) %>%
      #mutate(corrected_alignment_length = alignment_length)
      mutate(corrected_alignment_length = alignment_length-overlapping_length) 
  } else{
    tool_plasmid_data$overlapping_length<-0
    tool_plasmid_data <- tool_plasmid_data %>% mutate(corrected_alignment_length = alignment_length - overlapping_length)
  }
  
  ambiguous_path <- file.path(tooldir,'ambiguous_contigs.csv')
  
  if(file.exists(ambiguous_path)){
    ambiguous_alignments <-read.csv(ambiguous_path, sep=',', header=FALSE)
    names(ambiguous_alignments)<-c('strain','bin_name','reference_id','contig_name','contig_length','start','end')
    # ONLY GENERAL
    if(any(grepl(ambiguous_alignments$strain,pattern="\\."))){
    ambiguous_alignments <- ambiguous_alignments  %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
    }
    ambiguous_alignments_length_replicon<-ambiguous_alignments %>% 
      group_by(strain,bin_name,reference_id) %>% 
      summarise(ambiguous_length=sum(contig_length))
    #join wit the rest of the data
    tool_plasmid_data<-left_join(tool_plasmid_data, ambiguous_alignments_length_replicon,by=c("strain","bin_name","reference_id")) %>%
      mutate(ambiguous_length=replace_na(ambiguous_length,0))
    #return(quast_tag)
    #Filter out alignments in which the length is 0
    tool_plasmid_data <- tool_plasmid_data %>%
      filter(corrected_alignment_length>ambiguous_length)
  } else {
    tool_plasmid_data$ambiguous_length<-0
  }
  tool_plasmid_data <- tool_plasmid_data %>% 
    mutate(true_alignment_length = as.numeric(corrected_alignment_length - ambiguous_length))
    
  # potentially gplas specific
  if(toolname == "gplas2"){
    if(unbinned == FALSE){
    tool_plasmid_data<-tool_plasmid_data %>% 
      filter(!str_detect(bin_name, "Unbinned"))
    }
  }else if(toolname == "plasmidspades" || toolname =="mobsuite"){
    tool_plasmid_data<- tool_plasmid_data %>%
      mutate(bin_name = paste(strain,bin_name,sep="_"))
  }
 
  return(tool_plasmid_data)
  classifier_output <- quast_gplas_plasmidec_output
  quast_gplas_plasmidec_tag <- calculate_metrics(quast_output_raw, overlapping_path, quast_self_gfa_output, gplas_ambiguous_path)
  quast_gplas_plasmidec_tag_plasmids <- quast_gplas_plasmidec_tag %>% filter(classification=="plasmid")
  #4.6 REMOVE AMBIGUOUSLY ALIGNED DATA

  #Get ambiguously aligned regions - we will remove them from the alignment
  
  
  #Remove ambiguously aligned lengths
}
read_tool <- function(tooldir,toolname,benchmark_plasmids,unbinned=FALSE){
  #gplas_alignment_stats_path<-paste(gplas_output_directory,'alignment_statistics.csv',sep='/')
  quast_output_raw<-read.csv(file.path(tooldir,"alignment_statistics.csv"), sep=',', header=FALSE)
  names(quast_output_raw)<-c('strain','bin_name','reference_id','alignment_length','bin_length')
  quast_output_raw <- quast_output_raw %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
  #return(quast_output_raw)
  tool_plasmid_data <- benchmark_plasmids %>% 
    right_join(quast_output_raw,by=c('strain','reference_id')) %>%
    mutate(classification = ifelse(reference_id == 'no_correct_alignments','not_applicable',classification)) %>%
    mutate(classification = replace_na(classification,"chromosome")) %>%
    distinct()
  
  
  
  #gplas_overlapping_path<-paste(gplas_output_directory,'overlapping_length.csv',sep='/')
  overlapping_path <- file.path(tooldir,'overlapping_length.csv')
  
  if(file.exists(overlapping_path)){
    overlapping <-read.csv(overlapping_path, sep=',', header=FALSE)
    names(overlapping)<-c('strain','bin_name','reference_id','overlapping_length')
    overlapping <- overlapping %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
    tool_plasmid_data <-left_join(tool_plasmid_data,overlapping,by=c('strain','bin_name','reference_id')) %>%
      mutate(overlapping_length = replace_na(overlapping_length,0)) %>%
      #mutate(corrected_alignment_length = alignment_length)
      mutate(corrected_alignment_length = alignment_length-overlapping_length) 
  } else{
    tool_plasmid_data$overlapping_length<-0
    tool_plasmid_data <- tool_plasmid_data %>% mutate(corrected_alignment_length = alignment_length - overlapping_length)
  }
  
  ambiguous_path <- file.path(tooldir,'ambiguous_contigs.csv')
  
  if(file.exists(ambiguous_path)){
    ambiguous_alignments <-read.csv(ambiguous_path, sep=',', header=FALSE)
    names(ambiguous_alignments)<-c('strain','bin_name','reference_id','contig_name','contig_length','start','end')
    # ONLY GENERAL
    if(any(grepl(ambiguous_alignments$strain,pattern="\\."))){
      ambiguous_alignments <- ambiguous_alignments  %>% mutate(strain = sub("\\.\\d+.*$", "",strain))
    }
    ambiguous_alignments_length_replicon<-ambiguous_alignments %>% 
      group_by(strain,bin_name,reference_id,contig_name) %>% 
      mutate(contig_length = ceiling(mean(contig_length))) %>%
      summarise(ambiguous_length=sum(contig_length),
                subtracted_ambiguous_length = ambiguous_length-contig_length) %>%
      distinct() %>%
      group_by(strain,bin_name,reference_id) %>%
      summarise(ambiguous_length = sum(subtracted_ambiguous_length))
    #join wit the rest of the data
    tool_plasmid_data<-left_join(tool_plasmid_data, ambiguous_alignments_length_replicon,by=c("strain","bin_name","reference_id")) %>%
      mutate(ambiguous_length=replace_na(ambiguous_length,0))
    #return(quast_tag)
    #Filter out alignments in which the length is 0
    tool_plasmid_data <- tool_plasmid_data %>%
      filter(corrected_alignment_length>ambiguous_length)
  } else {
    tool_plasmid_data$ambiguous_length<-0
  }
  tool_plasmid_data <- tool_plasmid_data %>% 
    mutate(true_alignment_length = as.numeric(corrected_alignment_length - ambiguous_length))
  
  # potentially gplas specific
  if(toolname == "gplas2"){
    if(unbinned == FALSE){
      tool_plasmid_data<-tool_plasmid_data %>% 
        filter(!str_detect(bin_name, "Unbinned"))
    }
  }else if(toolname == "plasmidspades" || toolname =="mobsuite"){
    tool_plasmid_data<- tool_plasmid_data %>%
      mutate(bin_name = paste(strain,bin_name,sep="_"))
  }
  
  return(tool_plasmid_data)
  classifier_output <- quast_gplas_plasmidec_output
  quast_gplas_plasmidec_tag <- calculate_metrics(quast_output_raw, overlapping_path, quast_self_gfa_output, gplas_ambiguous_path)
  quast_gplas_plasmidec_tag_plasmids <- quast_gplas_plasmidec_tag %>% filter(classification=="plasmid")
  
}
separate_training <- function(strain_plasmids){
  set.seed(123)
  n_isolates <- strain_plasmids %>% select(strain) %>% distinct() %>% tally()
  strains <- strain_plasmids %>% select(strain) %>% distinct()
  
  benchmark_split<-sample_n(strains,n_isolates$n/2) %>% droplevels()
  benchmark_strains_gplas <- strain_plasmids %>% filter(strain %in% benchmark_split$strain)
  
  #`%!in%` = Negate(`%in%`)
  benchmark_strains_plasmidcc <- strain_plasmids %>% filter(!strain %in% benchmark_split$strain)
  
  #  as.data.frame(efm.strain_plasmids[benchmark_strains.tmp$strain %!in% benchmark_strains_gplas$strain,])
  names(benchmark_strains_plasmidcc)<-'strain'
  return(list(benchmark_strains_plasmidcc, benchmark_strains_gplas))
}

plot_metrics <- function(long_metrics,species) {
  
  
  
  all_arg_metrics_plot<-ggplot(long_metrics, aes(x=software, y=as.numeric(value)))+
    geom_boxplot()+
    geom_jitter(alpha = 0.3, aes(color = software)) + theme_bw() +
    scale_color_manual(values = gplas_palette) +
    theme(
      axis.title.x = element_text(size=0,face='bold'), axis.text.x=element_text(size=0,angle=45, hjust=1),
      axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=13,angle=0),
      legend.title = element_text(size = 0), legend.text = element_text(size = 18), legend.position = 'top')+#,panel.grid.major.x = element_blank()) +
    xlab('Software') + ylab('Value') +
    facet_grid(arg_plasmid~metric, scales='free_x') + theme(strip.text.x = element_text(size = 15),strip.text.y = element_text(size = 15))
  all_arg_metrics_plot  + ggtitle(paste(species,"metrics",sep=" "))
  
  #all_alignments_plasmids_long_large %>% filter(variable =="F1_score") %>% group_by(software) %>% summarize(amnt = n())
  
  
}

debug_toolmetrics <- function(){
classifier_output <- matrix(c(1,0,2,0,0,3,4,0,0,5,0,6,0,0,7,8,9,0,0,0),ncol=4,byrow=T) %>%
  as.data.frame() %>% 
  rownames_to_column("reference_id") %>%
  mutate(true_plasmid_length = c(3,7,11,15,9)) %>%
  pivot_longer(cols=c(V1,V2,V3,V4),names_to = "bin_name",values_to = "true_alignment_length") %>%
  group_by(bin_name) %>%
  mutate(bin_length = sum(true_alignment_length)) %>%
  ungroup() %>%
  mutate(software="testmatrix",
         accuracy = true_alignment_length/bin_length,
         completeness = true_alignment_length/true_plasmid_length) %>%
  group_by(bin_name) %>%
  mutate(repr_bin = ifelse(accuracy == max(accuracy),TRUE,FALSE)) %>%
  group_by(reference_id) %>%
  mutate(repr_plasmid = ifelse(completeness ==  max(completeness),TRUE,FALSE)) %>%
  pivot_longer(c(accuracy,completeness),names_to = "metric",values_to="value") %>%
  mutate(ambiguous_length = 0)
}  
#classifier_output <- out.full_metrics
#calculate_summed_metrics(classifier_output)  
summed_metrics_conditional <- function(classifier_output,length=TRUE,ARG=TRUE,unbinned=TRUE){
  plasmid_data <- classifier_output %>%
    ungroup() %>%
    select(strain,reference_id, true_plasmid_length, arg_plasmid,length_classification) %>%
    distinct() %>%
    group_by(arg_plasmid,length_classification) %>%
    mutate(grouplength = sum(true_plasmid_length)) 
  
  top_plasmids <- classifier_output %>%
    pivot_wider(names_from = metric, values_from=value) %>%
    group_by(software,reference_id) %>%
    slice(which.max(completeness)) %>%
    select(strain,reference_id, true_plasmid_length, arg_plasmid,length_classification, software,bin_name,true_alignment_length,completeness,accuracy)
  
  top_plasmids.w <- top_plasmids %>%
    select(strain,reference_id, true_plasmid_length,software, true_alignment_length) %>%
    pivot_wider(names_from=software, values_from = true_alignment_length)
  
  missing_gplas2 <- top_plasmids.w %>% filter(is.na(gplas2)) %>% ungroup() %>% select(strain) %>% distinct()# %>% left_join(top_plasmids.w)
  cat(missing_gplas2$strain, sep="\n")
  plasmid_metrics <- top_plasmids %>%
    #group_by(arg_plasmid) %>%
    group_by(arg_plasmid,length_classification) %>%
    mutate(numplas = n_distinct(reference_id)) %>%
    left_join(plasmid_data, by="reference_id",suffix = c("","P"))%>%
    group_by(software,arg_plasmid,length_classification) %>%
    #group_by(software,arg_plasmid) %>%
    mutate(rel_recall = sum(completeness)/numplas,
           recall = sum(true_alignment_length)/grouplength) %>% 
    select(software,arg_plasmid, length_classification, rel_recall,recall) %>%
    distinct()
  
  top_bins <- classifier_output %>%
    pivot_wider(names_from =  metric, values_from = value) %>%
    group_by(software, bin_name) %>%
    mutate(total_bin_length = sum(true_alignment_length)) %>%
    slice(which.max(accuracy)) %>%
    select(strain,reference_id, length_classification, arg_plasmid, bin_name,bin_length, software,true_alignment_length,completeness,accuracy)
  
  plasmid_metrics_p <- top_bins %>%
    group_by(software, arg_plasmid, length_classification) %>%
    mutate(numbin = n_distinct(bin_name)) %>%
    left_join(plasmid_data, by="reference_id",suffix = c("","P"))%>%
    group_by(software, arg_plasmid, length_classification) %>%
    mutate(totalbin = sum(true_alignment_length)) %>%
    mutate(rel_precision = sum(accuracy)/numbin)%>%
    group_by(software, arg_plasmid, length_classification) %>%
    mutate(precision = sum(true_alignment_length)/grouplength) %>%
    ungroup() %>%
    select(software,arg_plasmid,length_classification,rel_precision, precision) %>% distinct()
    
    
  total_plasmid_metrics <- plasmid_metrics %>%
    left_join(plasmid_metrics_p,by=c("software", "arg_plasmid", "length_classification")) %>%
    mutate(rel_F1 = (rel_recall*rel_precision)/(rel_recall+rel_precision)*2,
           F1 = (recall*precision)/(recall+precision)*2)
  
  return(total_plasmid_metrics)
  ggplot(plasmid_metrics,aes(x=software,y=rel_recall,fill=software)) + facet_wrap(length_classification~arg_plasmid) + geom_bar(stat="identity")
  ggplot(total_plasmid_metrics,aes(x=software,y=F1,fill=software)) + facet_wrap(length_classification~arg_plasmid) + geom_bar(stat="identity") +ylim(0,1)
  full_metrics <- classifier_output %>%
    group_by(software,length_classification, arg_plasmid) %>%
    select(-c(metric,value)) %>% distinct() %>%
    mutate(total_num = sum(true_alignment_length),
           total_ambiguous = sum(ambiguous_length),
           max_plas = ifelse(repr_plasmid, true_alignment_length,0),
           max_bin = ifelse(repr_bin, true_alignment_length,0)) %>%
    summarise(precision = sum(max_bin)/max(total_num),
              recall=sum(max_plas)/(max(full_plasmidome_length$total_plasmidome)-max(total_ambiguous))) %>%
    mutate(F1_score = 2*((precision*recall)/(precision+recall)))
}

calculate_summed_metrics <- function(classifier_output) {

  full_plasmidome_length <- classifier_output %>%
    ungroup() %>%
    select(reference_id,true_plasmid_length) %>%
    distinct() %>%
    summarise(total_plasmidome = sum(true_plasmid_length))
  num_plas <- classifier_output %>%
    ungroup() %>%
    select(reference_id) %>%
    distinct() %>%
    summarise(num_plas = n())
  

  # For each software, calculate the total alignment length and the amb. length
  # if the plasmid is the biggest in the bin, take it's length
  # if the bin contains the biggest plasmid fraction, take it's length
  # calculate precision as the fraction of biggest bin fractions over the 
  # total number of mapped bps, and recall as the biggest plasmid fractions over the
  # total plasmidome minus the ambiguous alignments
  full_metrics <- classifier_output %>%
    group_by(software) %>%
    select(-c(metric,value)) %>% distinct() %>%
    mutate(total_num = sum(true_alignment_length),
                           total_ambiguous = sum(ambiguous_length),
                           max_plas = ifelse(repr_plasmid, true_alignment_length,0),
                           max_bin = ifelse(repr_bin, true_alignment_length,0)) %>%
    summarise(precision = sum(max_bin)/max(total_num),
              recall=sum(max_plas)/(max(full_plasmidome_length$total_plasmidome)-max(total_ambiguous))) %>%
    mutate(F1_score = 2*((precision*recall)/(precision+recall)))
  #  
    full_metrics_p <- classifier_output %>%
      group_by(software) %>%
      pivot_wider(names_from = metric, values_from=value) %>%
      distinct() %>%
      mutate(num_bin = n_distinct(bin_name),
              total_acc = ifelse(repr_bin,accuracy, 0),
              total_comp = ifelse(repr_plasmid,completeness,0)) %>%
       summarise(recall = sum(total_comp)/max(num_plas$num_plas),
                 precision = sum(total_acc)/max(num_bin))%>%
       mutate(F1_score = 2*((precision*recall)/(precision+recall)))
      
  return(list(full_metrics,full_metrics_p))

}


calculate_ARI <- function(tool_output) {
  tool_df <- tool_output %>%
    mutate(true_alignment_length = as.numeric(true_alignment_length)) %>%
    select(reference_id, bin_name, true_plasmid_length, true_alignment_length) %>%
    distinct()
  tool_table <- tool_df %>%
    select(-true_plasmid_length) %>%
    group_by(reference_id, bin_name) %>%
    summarise(true_alignment_length = sum(true_alignment_length)) %>%
    spread(key = reference_id, value = true_alignment_length, fill = 0) %>%
    column_to_rownames("bin_name") %>% as.matrix()
  
  sumA <- sum(choose(tool_table, 2))
  sumC <- sum(choose(rowSums(tool_table), 2))
  sumD <- sum(choose(colSums(tool_table), 2))
  #N <- choose(sum(tool_df$true_plasmid_length), 2)
  N <- choose(sum(tool_table), 2)
  B <- (sumC * sumD) / N
  ARI <- (sumA - B) / (0.5 * (sumC + sumD) - B)
  
  return(ARI)
}
#speciesname="E_faecalis"
#argpath = file.path("../data/",speciesname,"/new/99_final_results/05_abricate/")
#toolname="MOB-suite"
#toolpath = file.path("../data/E_faecalis/new/99_final_results/08_quast_gplas_plasmidcc/")
#toolname="gplas2"

read_abricate <- function(argpath,toolpath,toolname,adjustment=FALSE,manual=FALSE){
  print("Import Gplas abricate output")
  filename <- switch(
    toolname,
    "gplas2" = "gplas_cc_abricate_output.csv",
    "MOB-suite" = "mob_abricate_output.csv",
    "plasmidSPAdes" = "spades_abricate_output.csv"
  )
  arg_bins<-read.csv(file.path(argpath,filename), sep='\t', header=TRUE)
  subset_names<- strsplit(as.character(arg_bins$X.FILE),'/')

  subset_names<-do.call(rbind, subset_names)
  if(toolname == "plasmidSPAdes" & adjustment == TRUE){
    subset_names <- subset_names %>%
      as.data.frame() %>%
      mutate(bin_name = ifelse(V13 == "bins",V14,V13))
    
    num_columns <- ncol(subset_names)
    set_names <- subset_names[,c(num_columns-3,num_columns)]
  }else if(toolname=="MOB-suite" & manual==TRUE){
    subset_names <- subset_names %>%
      as.data.frame() %>%
      mutate(bin_name = ifelse(grepl(".fasta",V13),V13,V14) )
    num_columns <- ncol(subset_names)
    set_names <- subset_names[,c(num_columns-3,num_columns)]
  }else{
  num_columns<-ncol(subset_names)
  set_names<-subset_names[,c(num_columns-1,num_columns)]
  }
  #if (species=='E. coli'){
  #  gplas_names<-as.data.frame(gplas_names)
  #  names(gplas_names)<-c("software","bin_name")
  #  gplas_names<-separate(gplas_names,"bin_name",c("strain","the_rest"), sep="_b",remove=FALSE)
  #  gplas_names<-gplas_names[,-c(1,4)]
  #  gplas_names<-gplas_names[,c(2,1)]
  #}
  arg_bins_clean<-data.frame(set_names,arg_bins[,-c(1)])
  colnames(arg_bins_clean)[c(1,2,3)]<-c('strain','bin_name','contig_name')
  arg_bins_clean <- arg_bins_clean %>%
    mutate(software=toolname) %>%
    mutate(strain = sub("\\.\\d+.*$", "",strain)) %>%
    mutate(bin_name = sub(".fasta","",bin_name)) %>%
    mutate(contig_name = gsub(":","_",contig_name)) %>%
    distinct()

  contig_names<-read.csv(file.path(toolpath,'contigs_references.csv'), sep=',', header=FALSE,fill=TRUE, 
                         col.names=c('strain','bin_name','reference_id','contig_name','contig_length','alignment_fraction','contig_start','contig_end')) %>%
    mutate(software= toolname) %>%
    mutate(strain= sub("\\.\\d+.*$", "",strain)) %>%
    distinct()
  tmp <- anti_join(arg_bins_clean,contig_names,by=c('strain','bin_name','contig_name','software'))
  arg_bins_references <- inner_join(arg_bins_clean,contig_names,by=c('strain','bin_name','contig_name','software')) %>%
    mutate(contig_start = as.numeric(contig_start)) %>%
    mutate(contig_end = as.numeric(contig_end)) %>%
    mutate(status_alignment = ifelse(START >= contig_start & END <= contig_end,'correct','incorrect'))%>%
    mutate(status_alignment = ifelse(is.na(contig_start),'correct',status_alignment)) %>%
    filter(status_alignment == "correct")
    
  return(arg_bins_references)
  
}

plotARGs <- function(tools_detected){
  arg_genes_location_plot<-ggplot(tools_detected, aes(x=origin, y=arg_numbers, fill=name)) + geom_bar(stat='identity',position='stack') +
    scale_fill_manual(values=c("#434343","#a6a6a6","#FF9933")) +  
    theme_bw()+
    theme(plot.title = element_text(size=18,hjust = 0.5,face='bold'),
          axis.title.x = element_text(size=14,face='bold'), axis.text.x=element_text(size=10,angle=0, hjust=0.5,vjust=1),
          axis.title.y=element_text(size=16,face="bold"), axis.text.y=element_text(size=14,angle=0),
          legend.title = element_text(size = 12, face='bold'),
          legend.text = element_text(size = 10),
          legend.position='top')  +
    xlab('ARG true origin') +
    ylab('Nr. ARGs') +
    labs(fill='ARG classification') +
    facet_wrap(~software,ncol=3) + theme(strip.text.x = element_text(size = 13))
  arg_genes_location_plot
  return(arg_genes_location_plot)
}

plot_contam <- function(all_contam,addbar=F){
  chromosome_contamination_plot<-ggplot(all_contam, aes(x=software, y=as.numeric(contam_bp)))+
    geom_boxplot()+
    geom_jitter(alpha = 0.3, aes(color = software)) + theme_bw() +
    scale_color_manual(values = gplas_palette) +
    theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
          axis.title.x = element_text(size=14,face='bold'), axis.text.x=element_text(size=0,angle=45, hjust=1),
          axis.title.y=element_text(size=14,face="bold"), axis.text.y=element_text(size=12,angle=0),
          legend.title = element_text(size = 0), legend.text = element_text(size = 14), legend.position = 'top')+#,panel.grid.major.x = element_blank()) +
    xlab('Software') + ylab('Chromosome contamination') #+
  #facet_grid(arg_plasmid~variable, scales='free_x') + theme(strip.text.x = element_text(size = 15),strip.text.y = element_text(size = 15))
  
  if(addbar){
    barplot <- ggplot(data = all_contam, aes(x = factor(software),fill=software)) +
      geom_bar() +
      scale_fill_manual(values=gplas_palette) +
      theme_bw() +
      theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
                                                      axis.title.x = element_text(size=0,face='bold'), axis.text.x=element_text(size=0,angle=45, hjust=1),
                                                      axis.title.y=element_text(size=14,face="bold"), axis.text.y=element_text(size=12,angle=0),
                                                      legend.title = element_text(size = 0), legend.text = element_text(size = 14), legend.position = 'top')+#,panel.grid.major.x = element_blank()) +
      xlab('Software') + ylab('Number of contaminated predictions') + 
      theme(legend.position="none")
    
    chromosome_contamination_plot <- grid.arrange(barplot, chromosome_contamination_plot, heights = c(1, 3))

  }
  chromosome_contamination_plot
}

calculate_classification <- function(class_output){
  TP <- sum(class_output$Prediction %in% "Plasmid" & class_output$true_classification %in% "plasmid")
  FP <- sum(class_output$Prediction %in% "Plasmid" & class_output$true_classification %in% "chromosome")
  FN <- sum(class_output$Prediction %in% "Chromosome" & class_output$true_classification %in% "plasmid")
  
  precision <- TP / (TP + FP)
  recall <- TP / (TP+FN)
  F1 <- 2*((precision*recall)/(precision+recall))
  result_df <- data.frame(
    recall = recall,
    precision = precision,
    f1_score = F1,
    TP = TP,
    FP = FP,
    FN = FN
  )
  return(result_df)
}

plot_total_metrics <- function(class_metrics){
  class_metrics.plot <- class_metrics %>%
    ggplot(aes(x=tool,y=value, color=metric))+ geom_point(size=3, width=0.2, aes(color = metric)) + theme_bw() +
    scale_color_viridis(discrete=T) + facet_grid(metric~species) +
    theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
          axis.title.x = element_text(size=0,face='bold'), axis.text.x=element_text(size=14,angle=45, face='bold', hjust=1),
          axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=13,angle=0),
          legend.title = element_text(size = 0), legend.text = element_text(size = 12), legend.position = 'left')+#,panel.grid.major.x = element_blank()) +
    xlab('Software') + ylab('Value')  + scale_y_continuous(limits=c(0.5,1))
  class_metrics.plot 
  
}
#speciesname <- "E_faecalis"
full_analysis <- function(speciesname){
  # Read metadata
  strain_plasmids <- read_metadata(file.path("../data/",speciesname,"99_final_results/06_lengths/"))
  
  strain_plasmids_ARGs <- extract_arg_plasmids(strain_plasmids,file.path("../data/",speciesname,"new/99_final_results/05_abricate"))
  
  # Length distribution
  threshold_data <- data.frame(
    Species = c('E_coli', 'E_faecalis', 'E_faecium', 'K_pneumoniae', 'S_enterica', 'S_aureus', 'A_baumannii', 'general'),
    Threshold = c(18000, 18000, 18000, 18000, 18000, 8000, 30000, 16000)
  )
  
  cutoff <- threshold_data[speciesname,"Threshold"]
  strain_plasmids_ARGs <- efm.strain_plasmids_ARGs %>% 
    mutate(length_classification = ifelse(true_plasmid_length >= cutoff,"large","small"))
  ldplot <- plot_length_distribution(strain_plasmids_ARGs,speciesname,save=FALSE, outfile=file.path("../results/",speciesname,"arg_plasmid_lengths.jpg"))# Saving doesn't work atm
  #return(ldplot)
  
  # Contigmap
  contigmap <- get_contigmap(file.path("../data/",speciesname,"99_final_results/01_quast_self/"))
  strain_plasmids_ARGs_contigs <- strain_plasmids_ARGs %>% left_join(contigmap, by=c("strain","reference_id"))
  
  # Read tools
  out.gplas <- read_tool(file.path("../data/",speciesname,"/new/99_final_results/08_quast_gplas_plasmidcc/"),"gplas2",strain_plasmids_ARGs_contigs)
  out.gplas.m <- calculate_metrics(out.gplas,"gplas2",long=T)
 
  out.mobsuite <- read_tool(file.path("../data/",speciesname,"/99_final_results/03_quast_mobsuite/"),"mobsuite",strain_plasmids_ARGs_contigs)
  out.mobsuite.m <- calculate_metrics(out.mobsuite,'MOB-suite',long=T)

  out.plspades <- read_tool(file.path("../data/",speciesname,"/99_final_results/04_quast_plasmidspades/"),"plasmidspades",strain_plasmids_ARGs_contigs)
  out.plspades.m <- calculate_metrics(out.plspades, 'plasmidSPAdes',long=T)
 
  out.full_metrics <- rbind(out.gplas.m,out.mobsuite.m,out.plspades.m)
  
  
}
#ldplot <- full_analysis(speciesname)
#ldplot


plot_tool_metrics <- function(long_metrics,species,ARGs=FALSE){
  all_arg_metrics_plot<-ggplot(long_metrics, aes(x=software, y=as.numeric(value),fill=software))+
    geom_bar(stat="identity") + 
    #geom_jitter(alpha = 0.3, aes(color = software)) + theme_bw() +
    scale_fill_manual(values = gplas_palette) +
    theme_pubclean() + ylim(0,1) +
    theme(
      axis.title.x = element_text(size=18,face='bold'), axis.text.x=element_text(size=0,angle=45, hjust=1),
      axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=13,angle=0),
      legend.title = element_text(size = 0), legend.text = element_text(size = 18), legend.position = 'top')+#,panel.grid.major.x = element_blank()) +
    xlab('Software') + ylab('Value') +
     theme(strip.text.x = element_text(size = 15),strip.text.y = element_text(size = 15)) 
  all_arg_metrics_plot  <- all_arg_metrics_plot + ggtitle(paste(species,"metrics",sep=" "))
  if(ARGs){
    all_arg_metrics_plot <- all_arg_metrics_plot + facet_grid(arg_plasmid~metric, scales='free_x')
  }else{
    all_arg_metrics_plot <- all_arg_metrics_plot + facet_grid(~metric, scales='free_x')
  }
  return(all_arg_metrics_plot)
  
  #all_alignments_plasmids_long_large %>% filter(variable =="F1_score") %>% group_by(software) %>% summarize(amnt = n())
  
}


apply_choose2 <- function(x) {
  if (!is.na(x)) {
    return(choose(x, 2))
  } else {
    return(NA)
  }
}
divide1000 <-function(x){
  if(!is.na(x)){
    return(x/1000)
  }else{
    return(0)
  }
}
library(gtools)
library(gmp)
#install.packages("combinat")
#ARI_list <- list()
#x <- tool_matrices[[2]]
calc_ARI <- function(x){
  toolma <- x 
  
  #toolma <- apply(toolma, c(1,2),divide1000)
  sumperm <- sum(apply(toolma, c(1, 2), apply_choose2))
  rowperm <- sum(sapply(rowSums(toolma), apply_choose2))
  colperm <- sum(sapply(colSums(toolma),apply_choose2))
  expsum <- rowperm*colperm/choose(n,2)
  n<- sum(toolma)
  
  ARI_t <- sumperm - expsum
  ARI_n <- 0.5*(rowperm+colperm)-expsum
  
  ARI <- ARI_t/ARI_n
  return(ARI)
}