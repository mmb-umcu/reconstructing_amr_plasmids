---
title: "gplas_template"
author: "Jesse Kerkvliet"
date: "2023-11-14"
output: html_document
params:
  species: "S. enterica"
  speciesname: "S_enterica"
  new: TRUE
  adjusted: FALSE
  manual: FALSE
  newsamples: FALSE
---

```{r}
# Declaring parameters
manual <- params$manual
speciesname <- params$speciesname
adjusted <- params$adjusted
newsamples <- params$newsamples
```

# Automated gplas report for species `r params$species`
This document is automatically generated. Any manual adjustments will be done afterwards.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      cache = FALSE,
                      dev = c("svglite","png"),
                      dpi = 300)
source("metric_functions.R",local=knitr::knit_global())
#1. Load libraries
library(tidyverse)
library(ggplot2)
library(tidyr)
library(broom)
library(dplyr)
library(readxl)
library(reshape2)
library(viridis)
library(ggExtra)
library(gridExtra)
library(ggpubr)
library(svglite)
library(gmp)
```


```{r}
# Read metadata
  strain_plasmids <- read_metadata(file.path("../data/",speciesname,"99_final_results/06_lengths/")) %>% distinct()

if(params$new){
  strain_plasmids2 <- read_metadata(file.path("../data/",speciesname,"/new/99_final_results/06_lengths/"))
  strain_plasmids2 <- read_metadata(file.path("../data/",speciesname,"/99_final_results/06_lengths/"))
  strain_plasmids_ARGs <- extract_arg_plasmids(strain_plasmids2,file.path("../data/",speciesname,"99_final_results/05_abricate"))
}else{
  #print(file.path("../data/",speciesname,"99_final_results/05_abricate"))
    strain_plasmids_ARGs <- extract_arg_plasmids(strain_plasmids,file.path("../data/",speciesname,"99_final_results/05_abricate"))
}

knitr::kable(strain_plasmids_ARGs)
```
## Length distribution
```{r}

  threshold_data <- data.frame(
    row.names = c('E_coli', 'E_faecalis', 'E_faecium', 'K_pneumoniae', 'S_enterica', 'S_aureus', 'A_baumannii', 'general'),
    Threshold = c(18000, 18000, 18000, 18000, 18000, 8000, 30000, 16000)
  )
  
  length_cutoff <- threshold_data[speciesname,]

  strain_plasmids_ARGs <- strain_plasmids_ARGs %>%
    mutate(length_classification = base::ifelse(true_plasmid_length >= length_cutoff,"large","small"))
  

  ldplot <- plot_length_distribution(strain_plasmids_ARGs,params$species,save=FALSE, outfile=file.path("../results/",speciesname,"arg_plasmid_lengths.jpg"))# Saving doesn't work atm
  dir.create(file.path("../results/figures/", speciesname))
  svglite(file.path("../results/figures/",speciesname,"length_distribution.svg"))
  ldplot
  dev.off()
  ldplot


  write.table(strain_plasmids_ARGs,file=file.path("../results",speciesname,"length_distribution_data"))
``` 
  
```{r}
# Contigmap
  contigmap <- get_contigmap(file.path("../data/",speciesname,"99_final_results/01_quast_self/"))
  strain_plasmids_ARGs_contigs <- strain_plasmids_ARGs %>% left_join(contigmap, by=c("strain","reference_id"))

  # Read tools
  if(params$new){
  out.gplas <- read_tool(file.path("../data/",speciesname,"/new/99_final_results/08_quast_gplas_plasmidcc/"),"gplas2",strain_plasmids_ARGs_contigs,unbinned = FALSE)
  }else{
    out.gplas <- read_tool(file.path("../data/",speciesname,"/99_final_results/08_quast_gplas_plasmidcc/"),"gplas2",strain_plasmids_ARGs_contigs, unbinned=FALSE)
  }
  out.gplas.m <- calculate_metrics(out.gplas,"gplas2",long=T)

  if(params$new & newsamples ){
  out.mobsuite <- read_tool(file.path("../data/",speciesname,"new/99_final_results/03_quast_mobsuite/"),"mobsuite",strain_plasmids_ARGs_contigs)
  out.mobsuite.m <- calculate_metrics(out.mobsuite,'MOB-suite',long=T)

  out.plspades <- read_tool(file.path("../data/",speciesname,"new/99_final_results/04_quast_plasmidspades/"),"plasmidspades",strain_plasmids_ARGs_contigs)
  out.plspades.m <- calculate_metrics(out.plspades, 'plasmidSPAdes',long=T)
  }else{
     
  out.mobsuite <- read_tool(file.path("../data/",speciesname,"/99_final_results/03_quast_mobsuite/"),"mobsuite",strain_plasmids_ARGs_contigs)
  out.mobsuite.m <- calculate_metrics(out.mobsuite,'MOB-suite',long=T)

  out.plspades <- read_tool(file.path("../data/",speciesname,"/99_final_results/04_quast_plasmidspades/"),"plasmidspades",strain_plasmids_ARGs_contigs)
  out.plspades.m <- calculate_metrics(out.plspades, 'plasmidSPAdes',long=T)
  }
  
  
  out.full_metrics <- rbind(out.gplas.m,out.mobsuite.m,out.plspades.m)

```

## Per-plasmid and per-bin metrics 


```{r}

out.full_metrics_large <- out.full_metrics %>% filter(length_classification == "large")# %>% filter(arg_plasmid=="ARG-plasmid")

out.tool_metrics <- calculate_summed_metrics(out.full_metrics)
out.tool_metrics.a <- out.tool_metrics[[1]]  %>%
  pivot_longer(c(precision, recall,F1_score),names_to="metric",values_to="value")

out.tool_metrics.r <- out.tool_metrics[[2]] %>%
  pivot_longer(c(precision, recall,F1_score),names_to="metric",values_to="value")

out.full_metrics_plotable <- out.full_metrics_large %>%
    filter((metric == "accuracy" & repr_bin) | (metric == "completeness" & repr_plasmid))

#out.full_metrics_plotable_pe <- out.full_metrics_large %>% pivot_wider(values_from="value",names_from="metric") %>%
#  group_by(bin_name) %>%
#  mutate(compqual = ifelse(bin_length < length_cutoff/5,FALSE,TRUE)) #%>%
#  pivot_longer(c(accuracy,completeness), names_to="metric",values_to="value") %>%
#  filter((metric == "accuracy" & repr_bin & compqual) | (metric == "completeness" & repr_plasmid))# %>%

#plot_metrics(out.full_metrics_plotable_pe, species=params$species)  

metricsplot <- plot_metrics(out.full_metrics_plotable,species=params$species)
svglite(file.path("../results/figures/",speciesname,"_bin_plasmid_metrics.svg"))
metricsplot
dev.off()
metricsplot
#plot_metrics(out.full_metrics_plotable_pe, species=params$species)
```

```{r}
tool_metricsplot.a <- plot_tool_metrics(out.tool_metrics.a,species=params$species,ARGs =FALSE) 
tool_metricsplot.r <- plot_tool_metrics(out.tool_metrics.r,species=params$species,ARGs =FALSE) 
svglite(file.path("../results/figures/",speciesname,"_bin_toolwide_metrics.svg"))
tool_metricsplot.a

#tool_metricsplot.r
dev.off()
tool_metricsplot.a

#tool_metricsplot.r
```

# Number of ARGs predicted by each tool
```{r ARGs}
if(params$new){
args_gplas <- read_abricate(file.path("../data/",speciesname,"/new/99_final_results/05_abricate/"),
                            file.path("../data/",speciesname,"/new/99_final_results/02_quast_gplas"),
                            "gplas2")
args_mobsuite <- read_abricate(file.path("../data/",speciesname,"/new/99_final_results/05_abricate/"),
                               file.path("../data/",speciesname,"/new/99_final_results/03_quast_mobsuite/"),
                               "MOB-suite",adjustment=adjusted,manual=manual)

args_spades <- read_abricate(file.path("../data/",speciesname,"/new/99_final_results/05_abricate/"),
                             file.path("../data/",speciesname,"/new/99_final_results/04_quast_plasmidspades/"),
                             "plasmidSPAdes",adjustment=adjusted)
}else{
  args_gplas <- read_abricate(file.path("../data/",speciesname,"/99_final_results/05_abricate/"),
                            file.path("../data/",speciesname,"/99_final_results/02_quast_gplas"),
                            "gplas2")
args_mobsuite <- read_abricate(file.path("../data/",speciesname,"/99_final_results/05_abricate/"),
                               file.path("../data/",speciesname,"/99_final_results/03_quast_mobsuite/"),
                               "MOB-suite",adjustment=adjusted,manual=manual)

args_spades <- read_abricate(file.path("../data/",speciesname,"/99_final_results/05_abricate/"),
                             file.path("../data/",speciesname,"/99_final_results/04_quast_plasmidspades/"),
                             "plasmidSPAdes",adjustment=adjusted)
}
#read.table("../data/S_enterica/new/99_final_results/05_abricate/spades_abricate_output.csv")
args_tools <- rbind(args_gplas,args_mobsuite,args_spades) %>% distinct()
arg_tools_counts <- args_tools %>% 
  group_by(software,strain, bin_name, reference_id) %>% summarize(arg_count = n()) %>%
  left_join(strain_plasmids_ARGs,by=c("strain","reference_id")) %>%
  mutate(classification = ifelse(is.na(classification),"chromosome",classification)) %>%
  mutate(missing =arg_count - true_nr_args)
  #filter(classification=="plasmid")


arg_tools_bincount <- arg_tools_counts %>%
  group_by(software,strain) %>%
  summarize(total_args_in_bin=sum(arg_count)) %>%
  left_join(arg_tools_counts,by=c('software','strain'))

  
total_predicted_strains <- inner_join(strain_plasmids_ARGs, args_tools, by=c('strain','reference_id')) %>%
  select(c(strain,reference_id)) %>% 
  distinct() %>%
  left_join(strain_plasmids_ARGs,by=c('strain','reference_id')) 
args_tools_detected <- arg_tools_bincount %>%
  group_by(software,classification) %>% summarize(total_genes=sum(arg_count)) %>%
  pivot_wider(names_from = classification, values_from=total_genes) %>%
  mutate(missed_args = sum(total_predicted_strains$true_nr_args)-plasmid)

args_all <- arg_tools_bincount %>% select(-c(classification,length_classification, true_plasmid_length, arg_plasmid)) %>%
  mutate(bin_name = ifelse(software!="gplas2",paste(strain,bin_name,sep="_"),bin_name)) %>%
  left_join(out.full_metrics,by=c('strain','bin_name','reference_id','software','true_nr_args')) %>%
  filter(arg_plasmid=="ARG-plasmid") #%>%
  #mutate(percentage = round(()))


args_tools_detected <- args_tools_detected %>%
  pivot_longer(cols = c("plasmid","chromosome","missed_args"),values_to="arg_numbers") %>%
  mutate(percentage = round(arg_numbers/sum(total_predicted_strains$true_nr_args)*100,2)) %>%
  mutate(percentage = ifelse(name == "chromosome",NA,percentage)) %>%
  mutate(origin = ifelse(name== "chromosome","Chromosome","Plasmidome"))

plotARGs(args_tools_detected)

```
# Chromosome contamination
```{r}
out.gplas$software = "gplas2"
out.mobsuite$software = "MOB-suite"
out.plspades$software = "plasmidSPAdes"
out.contam <-
  rbind(out.gplas,out.mobsuite,out.plspades) %>%
  filter(classification == "chromosome") %>%
  mutate(accuracy = true_alignment_length/bin_length)

out.contam_bp <- out.contam %>%
  group_by(software) %>%
  summarize(contam_pred = n(), contam_bp = sum(alignment_length))
out.contam_bin <- out.contam %>%
  filter(accuracy == 1) %>%
  group_by(software) %>%
  summarize(contam_bins = n() )

out.contam_all <- left_join(out.contam_bp,out.contam_bin, by="software")


out.contam <- out.contam %>% mutate(contam_bp = true_alignment_length) #
out.contam %>% plot_contam(addbar = T)




```

# Some notes about this dataset

Something funky can be happening when running these. Some sanity checks:


```{r}
out.full_metrics %>% ungroup() %>% 
  select(strain,software) %>% distinct() %>%
  table() %>% as.data.frame() %>%
  pivot_wider(names_from = software, values_from = Freq) %>%
  #filter(gplas2 == 0 | "MOB-suite" == 0 | plasmidSPAdes == 0) %>%
  knitr::kable()
```

```{r}

```

Inspect this table to see if the number of missing samples is balanced for each tool. Perhaps gplas2 was not quite done running?


### Unbinned contigs from gplas

Gplas keeps contigs unbinned if no suitable bin is found. However, this is still part of the output. Usually the accuracy of the bin cannot be trusted, as it's a bin with all unbinnable contigs. However, we can check what kind of plasmids are in these unbinned bins.

```{r}
  # Read tools
  if(params$new){
  out.gplas.unf <- read_tool(file.path("../data/",speciesname,"/new/99_final_results/08_quast_gplas_plasmidcc/"),"gplas2",strain_plasmids_ARGs_contigs,unbinned = TRUE)
  }else{
    out.gplas.unf <- read_tool(file.path("../data/",speciesname,"/99_final_results/08_quast_gplas_plasmidcc/"),"gplas2",strain_plasmids_ARGs_contigs, unbinned=TRUE)
  }
  out.gplas.m.unf <- calculate_metrics(out.gplas.unf,"gplas2",long=T) %>%
    filter(str_detect(bin_name, "Unbinned")) %>%
    filter(repr_plasmid) %>%
    arrange(value)
  
doplot <- as.logical(nrow(out.gplas.m.unf) > 0)
```
```{r eval=doplot}
#ggplot(out.gplas.m.unf, aes(x=metric,y=value,fill=metric)) + geom_boxplot() + geom_jitter()
#reorder(out.gplas.m.unf)
unbinned_plot <- out.gplas.m.unf %>%
    ggplot(aes(x=reference_id,y=value, color=metric))+ geom_point(size=3, width=0.2, aes(color = metric)) + theme_bw() +
    scale_color_viridis(discrete=T) + facet_grid(arg_plasmid~., scales = "free") +
    theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
          axis.title.x = element_text(size=0,face='bold'), axis.text.x=element_text(size=14,angle=45, face='bold', hjust=1),
          axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=13,angle=0),
          legend.title = element_text(size = 0), legend.text = element_text(size = 12), legend.position = 'left')+#,panel.grid.major.x = element_blank()) +
    xlab('Software') + ylab('Value')  + scale_y_continuous(limits=c(0,1))
unbinned_plot
```

If there's no plot, there's no unbinned contigs.

```{r}
compared_metrics <- summed_metrics_conditional(out.full_metrics)
```

```{r,eval=F}

write.table(out.full_metrics,file.path("../results/",speciesname,"full_metrics.csv"))
write.table(compared_metrics)
```


# ARI and bin rigour

The last two metrics are ARI and binning rigour. 

```{r,eval=F}

tool_matrices <- list()

# Get unique values in the 'tool' column
unique_tools <- unique(out.full_metrics$software)
for (toolname in unique_tools) {
  subset_data <- subset(out.full_metrics, software == toolname)
  reshaped_data <- dcast(subset_data, bin_name ~ reference_id, value.var = "true_alignment_length", fun.aggregate = sum)

  reshaped_data <- reshaped_data[, -1]

  tool_matrices[[toolname]] <- reshaped_data
}

print(lapply(tool_matrices, calc_ARI))
#ARI <- out.full_metrics %>% calc_ARI()

View(tool_matrices$gplas2)
```

## binning rigour

```{r}
binrig <- out.full_metrics %>%
  group_by(software,strain) %>%
  summarise(nbin = n_distinct(bin_name), nplas = n_distinct(reference_id)) 

rateplot <- binrig %>%
  ggplot(aes(y=software, x=nbin-nplas, color=software)) +geom_vline(xintercept=0)+geom_boxplot()+ geom_point(position="jitter") + geom_density() 

rateplot+theme_minimal() + scale_color_manual(values=gplas_palette) +xlim(c(-10,10))
 

```

## Number of moderate+ quality bins
Based on CAMI
```{r}
out.full_metrics.w <- out.full_metrics %>%
  group_by(software,bin_name) %>%
  pivot_wider(values_from="value",names_from="metric")%>%
  filter(repr_bin==T) %>%
  mutate(bin_quality = case_when(
    (completeness > 0.9 & accuracy > 0.95) ~ "very high",
    (completeness > 0.8 & accuracy > 0.85) ~ "high",
    (completeness > 0.7 & accuracy > 0.75) ~ "moderate",
    (completeness > 0.6 & accuracy > 0.65) ~ "low",
    (completeness < 0.6 | accuracy < 0.65) ~ "very low"
  ))%>%
  select(software, bin_name, bin_quality) %>% distinct() %>%
  group_by(software,bin_quality) %>%
  summarise(category_total = n()) %>%
  group_by(software) %>%
  mutate(perc_category = category_total / sum(category_total))

ggplot(out.full_metrics.w, aes(software,y=perc_category,fill=factor(bin_quality,levels=c("very high","high","moderate","low",'very low')))) + geom_bar(stat="identity")
```
### quality for long args
```{r qualarg}
out.full_metrics_l.a <- out.full_metrics %>%
  filter(arg_plasmid=="ARG-plasmid" & length_classification=="large") %>%
  group_by(software,bin_name) %>%
  pivot_wider(values_from="value",names_from="metric")%>%
  filter(repr_bin==T) %>%
  mutate(bin_quality = case_when(
    (completeness > 0.9 & accuracy > 0.95) ~ "very high",
    (completeness > 0.8 & accuracy > 0.85) ~ "high",
    (completeness > 0.7 & accuracy > 0.75) ~ "moderate",
    (completeness > 0.6 & accuracy > 0.65) ~ "low",
    (completeness < 0.6 | accuracy < 0.65) ~ "very low"
  ))%>%
  select(software, bin_name, bin_quality,completeness,accuracy) %>% distinct() %>%
  group_by(software,bin_quality) %>%
  summarise(category_total = n()) %>%
  group_by(software) %>%
  mutate(perc_category = category_total / sum(category_total))

ggplot(out.full_metrics_l.a, aes(software,y=perc_category,fill=factor(bin_quality,levels=c("very high","high","moderate","low",'very low')))) + geom_bar(stat="identity")
```


# Writing all output data

```{r,echo=T,eval=F}
#DISABLED RUNNING
write.table(out.full_metrics, file=file.path("../results/",speciesname,'all_metrics_perbin.csv'))
write.table(out.full_metrics_plotable, file=file.path("../results/",speciesname,"all_metrics_large_perbin.csv"))

write.table(out.full_metrics.w, file=file.path("../results/",speciesname,"bin_quality_metrics.csv"))
write.table(out.full_metrics_l.a, file=file.path("../results",speciesname,"bin_quality_metrics_largs.csv"))
write.table(binrig, file=file.path("../results",speciesname,"binning_rigour.csv"))

write.table(out.contam, file=file.path('../results/',speciesname,'contamination.csv'))

write.table(args_tools_detected, file=file.path('../results/',speciesname,'args_detected.csv'))

write.table(total_predicted_strains, file=file.path('../results/',speciesname,'total_strains_predicted.csv'))

```
