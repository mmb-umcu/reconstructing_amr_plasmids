---
title: "Repeated_element_analysis"
author: "Julian Paganini"
date: "October 4, 2023"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(rmarkdown)
library(knitr)
library(ggplot2)
library(tidyr)
library(broom)
library(dplyr)
library(readxl)
library(reshape2)
library(tidyverse)
library(viridis)
```

#0. Get repeated elements from predictions
First, gather all repeated elements included in plasmid predictions from *_results.tab file and those predicted as chromosome. For this, I first run the following bash codes.
```{bash}
#grep Repeat */*results.tab | cut -f 2 -d / | sed 's/_results.tab:/ /g' >> ../99_final_results/10_gplas_repeat_predictions/plasmid_predicted_repeated_elements.tab

#grep Chromosome */*chromosome_repeats.tab | cut -f 2 -d / | sed 's/_chromosome_repeats.tab:/ /g' >> ../99_final_results/10_gplas_repeat_predictions/chromosome_predicted_repeated_elements.tab


```


#1. Input files
```{r}

#species name to show up in tables and plots
species<-'E. faecium'
#species directory name (to replace in importing and exporting of files)
species_dir<-'E_faecium'

#Obtained using the code above=====
plasmidome_repeats_raw_path<-paste('../results',species_dir,'10_gplas_repeat_predictions/plasmid_predicted_repeated_elements.tab', sep='/')
plasmidome_repeats_raw<-read.csv(plasmidome_repeats_raw_path, header=FALSE, sep=' ', col.names=c('isolate','contig_number','contig_name','prob_chromosome','prob_plasmid','prediction', 'length','coverage',"bin"))

chromosome_repeats_raw_path<-paste('../results',species_dir,'10_gplas_repeat_predictions/chromosome_predicted_repeated_elements.tab', sep='/')
chromosome_repeats_raw<-read.csv(chromosome_repeats_raw_path, header=FALSE, sep=' ', col.names=c('isolate','contig_number',"origin_prediction"))
#=========


#Benchmarking plasmids metadata
plasmid_ids_path<-paste('../results',species_dir,'06_lengths/strains_plasmids.csv', sep='/')
plasmid_ids<-read.table(plasmid_ids_path, sep=',', header=FALSE, col.names = c('strain_id','reference_id'))
plasmid_ids$classification<-'plasmid'
plasmid_ids<-plasmid_ids[,c(2,3)]

#Obtained from gather_quast_results.py (gplas)=======

#List of isolates that have gplas predictions
gplas_isolates_predictions_path<-paste('../results',species_dir,'08_quast_gplas_plasmidcc/alignment_statistics.csv', sep='/')
gplas_isolates_predictions<-read.csv(gplas_isolates_predictions_path, sep=',', header=FALSE,fill=TRUE, col.names=c('strain','bin_name','reference_id','alignment_length','bin_length'))

#Obtained from gather_quast_results.py (self-alignment)=======
self_contigs_path<-paste('../results',species_dir,'01_quast_self/contigs_references.csv', sep='/')

if (species =='E. coli'){
  self_contigs_alignments<-read.csv(self_contigs_path, sep=',', header=FALSE,fill=TRUE, col.names=c('strain','reference_id','contig_name','contig_length','alignment_fraction','contig_start','contig_end'))  
  self_contigs_alignments<-left_join(self_contigs_alignments,plasmid_ids,by='reference_id')
  count_recovered_plasmids<-self_contigs_alignments[,c(1,2,8)] %>% filter(classification=='plasmid') %>% unique()
}else if (species=='General') {
  self_contigs_alignments<-read.csv(self_contigs_path, sep=',', header=FALSE,fill=TRUE, col.names=c('strain','bin_name','reference_id','contig_name','contig_length','alignment_fraction','contig_start','contig_end'))
  self_contigs_alignments<-left_join(self_contigs_alignments,plasmid_ids,by='reference_id')
  #self_contigs_alignments<-inner_join(self_contigs_alignments,benchmark_strains,by='strain')
  count_recovered_plasmids<-self_contigs_alignments[,c(1,3,9)] %>% filter(classification=='plasmid') %>% unique()
}else{
  self_contigs_alignments<-read.csv(self_contigs_path, sep=',', header=FALSE,fill=TRUE, col.names=c('strain','bin_name','reference_id','contig_name','contig_length','alignment_fraction','contig_start','contig_end'))
  self_contigs_alignments<-left_join(self_contigs_alignments,plasmid_ids,by='reference_id')
  count_recovered_plasmids<-self_contigs_alignments[,c(1,3,9)] %>% filter(classification=='plasmid') %>% unique()
}
self_contigs_alignments$classification<-as.character(self_contigs_alignments$classification)
self_contigs_alignments$classification[is.na(self_contigs_alignments$classification)]<-'chromosome'

#===============

#Obtained by "cat"-ing all plasmidCC predictions | adding the isolate name at the end
#(detail script better)
plasmidcc_classification_path<-paste('../results',species_dir,'09_plasmidCC_output/all_plasmidCC_classifications.tsv', sep='/')
plasmidcc_classification<-read.csv(plasmidcc_classification_path, sep='\t', header=FALSE)
names(plasmidcc_classification)<-c("contig_name","nr_chromosome_matches","nr_plasmid_matches","nr_unclassified","total_matches","chromsome_fraction","plasmid_fraction","final_classification","isolate")



```



#2. Wrange data from predicted repeated elemets

```{r}
#Get the repeats predicted to be in the Plasmidome
plasmidome_repeats_raw$species<-species
plasmidome_repeats_raw$origin_prediction<-'plasmid'
#clean for merge
plasmidome_repeats<-plasmidome_repeats_raw[,c(1,2,10,11)]


#Get the repeats predicted to be in the Chromsome
chromosome_repeats_raw$species<-species
chromosome_repeats_raw$origin_prediction<-'chromosome'

#Merge chr and plasmid data in long-format
all_predicted_repeats<-rbind(plasmidome_repeats,chromosome_repeats_raw)#,by=c('isolate','contig_number','species'))

#Get it into wide format
all_predicted_repeats_wide<-dcast(all_predicted_repeats, isolate + contig_number + species~origin_prediction, fun.aggregate = length)

#Tag it as different categories for repeats (plasmid, chromosome, ambiguous)
all_predicted_repeats_wide$repeat_category_predicted<-ifelse(all_predicted_repeats_wide$chromosome==0,'repeat plasmid',ifelse(all_predicted_repeats_wide$plasmid==0,'repeat chromosome','ambiguous'))
all_predicted_repeats_wide$contig_number<-as.character(all_predicted_repeats_wide$contig_number)

#Count how many different types of predicted repeat we have
count_predicted_repeat_types<-all_predicted_repeats_wide %>% group_by(repeat_category_predicted) %>% summarise(count=n())

```

#3. Get ground truth data
This data is obtained from every species by using the scritp 'get_individual_metrics'
```{r}

if (species=='E. faecium' || species=="E. faecalis"){
  #replace name of isolates
  self_contigs_alignments$strain <- sub("_(.*?)_.*$", "_\\1", self_contigs_alignments$strain)
}

#Identify Ambiguously aligned contigs
self_contigs_alignments<-self_contigs_alignments %>% group_by(strain,contig_name) %>% mutate(count=n())
#Filter out misasemblies (which might be confused with ambiguosly aligned contigs)
self_contigs_alignments<-self_contigs_alignments[is.na(self_contigs_alignments$contig_start),]

#10.4 Sub-classify ambiguous contigs (all-plasmid, all-chromsome, real-ambiguous)
self_contigs_alignments_wide<-dcast(self_contigs_alignments, strain+contig_name~classification,value.var ='count')
self_contigs_alignments_wide$true_classification<-ifelse(self_contigs_alignments_wide$chromosome==0 & self_contigs_alignments_wide$plasmid==1,'plasmid',ifelse(self_contigs_alignments_wide$chromosome==1 & self_contigs_alignments_wide$plasmid==0,'chromosome',ifelse(self_contigs_alignments_wide$chromosome>1 & self_contigs_alignments_wide$plasmid==0,'repeat chromosome',ifelse(self_contigs_alignments_wide$plasmid>1 & self_contigs_alignments_wide$chromosome==0,'repeat plasmid','ambiguous'))))

#Export data
true_contig_classification<-self_contigs_alignments_wide
#true_contig_classification<-read.csv('../results/E_coli/outputs/individual_contig_classification.csv', sep=',', header=TRUE)
#separate number into names
true_contig_classification<-separate(true_contig_classification,'contig_name',into=c('contig_number','the_rest',sep='_'))
true_contig_classification<-true_contig_classification[,-c(3,4)]
true_contig_classification$contig_number<-gsub(true_contig_classification$contig_number,pattern="S",replacement="")
colnames(true_contig_classification)[c(1,3,4)]<-c('isolate','true_chromosome_occurences','true_plasmidome_occurences')
```

#4. Combine both datasets
```{r}
contig_classification_predictions<-left_join(true_contig_classification,all_predicted_repeats_wide,by=c('isolate','contig_number'))
```


#6. Calculate metrics for repeated elements
```{r}
#Remove the the isolates for which no plasmid predictions were made
contig_classification_predictions<-contig_classification_predictions[contig_classification_predictions$isolate %in% gplas_isolates_predictions$strain, ]

#Find false negatives
#Get all contigs that are repeats but have no prediction - FNs
repeats_fn<-contig_classification_predictions[(contig_classification_predictions$true_classification=='repeat plasmid' | contig_classification_predictions$true_classification=='repeat chromosome' | contig_classification_predictions$true_classification=='ambiguous') & is.na(contig_classification_predictions$repeat_category_predicted), ]

#Get all things that are repeats and do have a prediction
repeats_predicted<-contig_classification_predictions[!is.na(contig_classification_predictions$repeat_category_predicted),]

#Combine the last two
all_repeats_classifications<-rbind(repeats_fn,repeats_predicted)

#Add a label for not identified repats
all_repeats_classifications$repeat_category_predicted[is.na(all_repeats_classifications$repeat_category_predicted)]<-'Not identified'

```

#6.Define metrics for repeat evaluation
True tipes of repeats
-Repeeats in chromosome (these were are not interested in, right? - well, I'm interested in the sense that is an error)
-Repeats in plasmids
-Repeat in both

Recall= TP/TP+FN ; Precision= TP/TP+FP
TP=Repeats predicted as plasmid or ambiguous, and that in fact are REPEATS plasmid or ambiguoues. Also contigs that are not repeats but that are plasmid-unitigs.
FP=Repeats predicited as plasmid or ambiguous, but that in fact are either chromosomal or are not repeats at all
FN=Repeats predicted as chromosome, but that in fact are plasmid or ambiguous. Also include plasmid or ambiguous repeats that were not identified as repeats by gplas

```{r}

all_repeats_classifications_interest<-all_repeats_classifications
#For true ambiguous contigs
#TP
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='ambiguous' & all_repeats_classifications_interest$repeat_category_predicted=='ambiguous','TP',NA)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='ambiguous' & all_repeats_classifications_interest$repeat_category_predicted=='repeat plasmid','TP',all_repeats_classifications_interest$metric_classification)
#FN
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='ambiguous' & all_repeats_classifications_interest$repeat_category_predicted=='Not identified','FN',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='ambiguous' & all_repeats_classifications_interest$repeat_category_predicted=='repeat chromosome','FN',all_repeats_classifications_interest$metric_classification)

#TRUE PLASMID REPEATS===================================
#TP
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='repeat plasmid','TP',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='ambiguous','TP',all_repeats_classifications_interest$metric_classification)

#FN
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='repeat chromosome','FN',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='Not identified','FN',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='repeat chromosome','FN',all_repeats_classifications_interest$metric_classification)

#==================================================================================================================

#True chromosome repeats - maybe it will be removed later
#FN
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='Not identified','FN',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='Not identified','FN',all_repeats_classifications_interest$metric_classification)
#TN
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='repeat chromosome','TN',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='repeat chromosome','TN',all_repeats_classifications_interest$metric_classification)

#FP
#Things that are not repeats -> true_classification=='chromosome'
#Things that are: true_classification==repeat chromsome, prediction=Plasmidome_fraction or  Plas_Chr_repeat
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='repeat plasmid', 'FP',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='ambiguous', 'FP',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='repeat plasmid', 'FP',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='chromosome' & all_repeats_classifications_interest$repeat_category_predicted=='ambiguous', 'FP',all_repeats_classifications_interest$metric_classification)
#all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='repeat chromosome', 'FP',all_repeats_classifications_interest$metric_classification)

#======PLASMID UNITIGS =========================================
#TP
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='repeat plasmid','TP',all_repeats_classifications_interest$metric_classification)
all_repeats_classifications_interest$metric_classification<-ifelse(all_repeats_classifications_interest$true_classification=='plasmid' & all_repeats_classifications_interest$repeat_category_predicted=='ambiguous','TP',all_repeats_classifications_interest$metric_classification)



```


#7. Calculate metrics and export table
```{r}
summary_binary_stats<-all_repeats_classifications_interest %>% group_by(metric_classification) %>% summarise(count=n())
summary_binary_stats$species<-species
summary_binary_stats

repeat_general_summary_path<-paste('../results',species_dir,'repeats_results_summary/repeats_classification_general_summary.tsv',sep='/')
repeat_general_summary_path

write.table(summary_binary_stats,repeat_general_summary_path,row.names = FALSE, sep='\t')



```

#8. Plot compositions of TP, FN, FP
```{r fig.width=4, fig.height=6, dpi=300}
plot_summary_classifications<- all_repeats_classifications_interest %>% group_by(metric_classification,true_classification) %>% summarise(count=n())

#re-order categories for plotting
plot_summary_classifications$true_classification<-factor(plot_summary_classifications$true_classification,levels=c('ambiguous','repeat plasmid','repeat chromosome', 'plasmid', 'chromosome'))

if (species=='S. aureus'){
  ggplot(plot_summary_classifications, aes(x=metric_classification, y=count, fill=metric_classification)) + geom_bar(stat='identity',position='stack') + 
  theme_minimal()+
  scale_fill_manual(values=c("#ffa700","#0057e7")) +
  theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
        axis.title.x = element_text(size=16), axis.text.x=element_text(size=12),
        axis.title.y=element_text(size=16), axis.text.y=element_text(size=12), 
        legend.title = element_text(size = 14), 
        legend.text = element_text(size = 12),
        legend.position='top')  +
   xlab('True contig classification') + 
  labs(fill='Prediction') +
  ylab('Nr. contigs') #+
  #scale_y_continuous(breaks=seq(0, 2, by = 1)) +
  #facet_grid(variable~phylogroup,scales='free_x') + theme(strip.text.x = element_text(size = 20), strip.text.y = element_text(size = 18))
  
}else {

ggplot(plot_summary_classifications, aes(x=metric_classification, y=count, fill=metric_classification)) + geom_bar(stat='identity',position='stack') + 
  theme_minimal()+
  scale_fill_manual(values=c("#ffa700","#d62d20","#0057e7","#008744")) +
  theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
        axis.title.x = element_text(size=16), axis.text.x=element_text(size=12),
        axis.title.y=element_text(size=16), axis.text.y=element_text(size=12), 
        legend.title = element_text(size = 14), 
        legend.text = element_text(size = 12),
        legend.position='top')  +
   xlab('True contig classification') + 
  labs(fill='Prediction') +
  ylab('Nr. contigs') #+
  #scale_y_continuous(breaks=seq(0, 2, by = 1)) +
  #facet_grid(variable~phylogroup,scales='free_x') + theme(strip.text.x = element_text(size = 20), strip.text.y = element_text(size = 18))
}

```



#9. Make a matrix for each combination.
```{r fig.width=8, fig.height=6, dpi=300}
repeat_classification_frequency<-table(all_repeats_classifications_interest$true_classification, all_repeats_classifications_interest$repeat_category_predicted)
repeat_classification_frequency
if (species=='E. faecalis'){
repeat_classification_frequency<-repeat_classification_frequency[,c(1,3,2)]
repeat_classification_frequency<-repeat_classification_frequency[c(1,5,4,2,3),]
} else if (species=='S. aureus') {
 repeat_classification_frequency<-repeat_classification_frequency
} else {
repeat_classification_frequency<-repeat_classification_frequency[,c(1,3,4,2)]
repeat_classification_frequency<-repeat_classification_frequency[c(1,4,5,2,3),]  
}
repeat_classification_frequency

repeat_classification_fraction <- prop.table(repeat_classification_frequency, margin = 1)
repeat_classification_fraction

#plot in ggplot
heatmap_data <- all_repeats_classifications_interest %>% group_by(true_classification,repeat_category_predicted,metric_classification ) %>% summarise(frequency=n())

heatmap_data$true_classification<-factor(heatmap_data$true_classification,levels=c('chromosome','plasmid','repeat chromosome','repeat plasmid','ambiguous'))

heatmap_data$repeat_category_predicted<-factor(heatmap_data$repeat_category_predicted,levels=c('ambiguous','repeat plasmid','repeat chromosome', 'plasmid', 'chromosome', 'Not identified'))

heatmap_data_path<-paste('../results',species_dir,'repeats_results_summary/heat_map_data.tsv',sep='/')
heatmap_data$species<-species
write.table(heatmap_data,heatmap_data_path,row.names = FALSE, sep='\t')

ggplot(heatmap_data, aes(x = repeat_category_predicted, y = true_classification, fill = metric_classification)) +
  geom_tile(color='white') +
  geom_text(aes(label=frequency)) +
  scale_fill_manual(values=c("#ffa700","#d62d20","#0057e7","#008744")) +
  theme_minimal() +
  theme(panel.grid = element_blank(),
        axis.title.x = element_text(size=16), axis.text.x=element_text(size=12),
        axis.title.y=element_text(size=16), axis.text.y=element_text(size=12), 
        legend.title = element_text(size = 14), 
        legend.text = element_text(size = 12),
        legend.position='top') +
  labs(title = "") +
  ylab('True contig classification') +
  xlab('gplas2 prediction') +
  labs(fill='Prediction classification') 
```


#10. Plot the lengths of different classes of contigs 
```{r fig.width=8, fig.height=5, dpi=300}
#import data of contigs
#all_contigs_info_raw<-read.csv('../results/E_coli/01_quast_self/contigs_references.csv', sep=',', header=FALSE)
#all_contigs_info<-all_contigs_info_raw[,c(1,3,4)]
#names(all_contigs_info)<-c('isolate','contig_name','contig_length')

#wrangle the name of contigs
all_contigs_info<-self_contigs_alignments[,c(1,4,5)]
colnames(all_contigs_info)[1]<-'isolate'
all_contigs_info<-separate(all_contigs_info,'contig_name',into=c('contig_number','the_rest',sep='_'))
all_contigs_info<-all_contigs_info[,-c(3,4)]
all_contigs_info$contig_number<-gsub(all_contigs_info$contig_number,pattern="S",replacement="")
all_contigs_info<-unique(all_contigs_info)

#merge with data of contigs
all_repeats_classifications_interest<-left_join(all_repeats_classifications_interest,all_contigs_info,by=c('isolate','contig_number'))

#Analyze the distribution of lenghts
length_distribution_by_class<-all_repeats_classifications_interest %>% group_by(metric_classification) %>% summarise( min_contig_length=min(contig_length),mean_contig_length=mean(contig_length),median_contig_length=median(contig_length),iqr=IQR(contig_length), max_contig_length=max(contig_length))
length_distribution_by_class

all_repeats_classifications_interest$log_contig_length<-log(all_repeats_classifications_interest$contig_length,10)

repeats_length_classification_plot<-ggplot(all_repeats_classifications_interest, aes(x=metric_classification, y=as.numeric(log_contig_length)))+
  geom_boxplot()+
  geom_jitter(alpha = 0.3, aes(color = true_classification)) + theme_bw() +
  #scale_color_manual(values = c('#FFC300','#00C5FF','#00FF93','#F23CB3','#443F42')) + 
  theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
        axis.title.x = element_text(size=20,face='bold'), axis.text.x=element_text(size=20,angle=0),
        axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=14,angle=0),
        legend.title = element_text(size = 14), legend.text = element_text(size = 10), legend.position = 'right')+#,panel.grid.major.x = element_blank()) +
   xlab('Classification') + ylab('log(contig length)') +
  guides(colour = guide_legend('Contig true origin',override.aes = list(size = 5))) #+
  #facet_grid(length_classification~variable) + theme(strip.text.x = element_text(size = 16),strip.text.y = element_text(size = 16))

repeats_length_classification_plot


```

#11. Evaluate binary classificaiton probabilities by Centrifuge

```{r fig.width=10, fig.height=5, dpi=300}
#plasmidcc_classification<-read.csv('../results/E_coli/09_all_plasmidEC_classifications/plasmidCC_output.tsv', sep='\t', header=FALSE)
names(plasmidcc_classification)<-c("contig_name","nr_chromosome_matches","nr_plasmid_matches","nr_unclassified","total_matches","chromsome_fraction","plasmid_fraction","final_classification","isolate")
#clean the name of the contig
plasmidcc_classification<-separate(plasmidcc_classification,'contig_name',into=c('contig_number','the_rest',sep='_'))
plasmidcc_classification<-plasmidcc_classification[,-c(2,3)]
plasmidcc_classification$contig_number<-gsub(plasmidcc_classification$contig_number,pattern="S",replacement="")
plasmidcc_classification<-unique(plasmidcc_classification)

#now match plasmidCC classifications with the data from the repeats
all_repeats_classifications_interest<-left_join(all_repeats_classifications_interest, plasmidcc_classification,by=c('isolate','contig_number'))

#Plot the same variables (TP, TN, FP) in the X axis. THen plot the plasmid classification in the Y-axis. Get two lines to denote plasmid, unclassified and chromosme classification

repeats_plasmidCC_score_plot<-ggplot(all_repeats_classifications_interest, aes(x=metric_classification, y=as.numeric(plasmid_fraction)))+
  geom_boxplot(colour='grey')+
  geom_jitter(alpha = 0.3, aes(color=true_classification, shape = final_classification)) +
 theme_bw() +
  geom_hline(yintercept = 0.3, linetype = 'dotted') +
  geom_hline(yintercept = 0.7, linetype = 'dotted') +
  #scale_color_manual(values = c('#FFC300','#00C5FF','#00FF93','#F23CB3','#443F42')) + 
  theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
        axis.title.x = element_text(size=20,face='bold'), axis.text.x=element_text(size=20,angle=0),
        axis.title.y=element_text(size=18,face="bold"), axis.text.y=element_text(size=14,angle=0),
        legend.title = element_text(size = 14), legend.text = element_text(size = 10), legend.position = 'right')+#,panel.grid.major.x = element_blank()) +
   xlab('Classification') + ylab('plasmidCC Score') +
  guides(shape = guide_legend('PlasmidCC classification',override.aes = list(size = 5)), colour = guide_legend('True contig origin',override.aes = list(size = 5))) #+

repeats_plasmidCC_score_plot

all_repeats_classifications_interest$species <- species

all_repeats_classifications_interest_path<-paste('../results',species_dir,'repeats_results_summary/length_plasmidCC_ambiguous_classifications.tsv',sep='/')
write.table(all_repeats_classifications_interest,all_repeats_classifications_interest_path,row.names = FALSE, sep='\t')

```


#12. Lentgh vs plasmidCC score
```{r fig.width=10, fig.height=5, dpi=300}

repeats_prediction_score_plot<-ggplot(all_repeats_classifications_interest, aes(x=as.numeric(log_contig_length), y=plasmid_fraction))+
  geom_point(alpha = 0.3, size=3, aes(color = true_classification, shape=final_classification)) + 
  theme_bw() +
  geom_hline(yintercept = 0.3, linetype = 'dashed') +
  geom_hline(yintercept = 0.7, linetype = 'dashed') +
  #scale_color_manual(values = c('#FFC300','#00C5FF','#00FF93','#F23CB3','#443F42')) + 
  theme(plot.title = element_text(size=0,hjust = 0.5,face='bold'),
        axis.title.x = element_text(size=14,face='bold'), axis.text.x=element_text(size=10,angle=0),
        axis.title.y=element_text(size=14,face="bold"), axis.text.y=element_text(size=10,angle=0),
        legend.title = element_text(size = 12), legend.text = element_text(size = 10), legend.position = 'right')+#,panel.grid.major.x = element_blank()) +
   xlab('log(contig length)') + ylab('plasmidCC score') +
  guides(shape = guide_legend('PlasmidCC classification',override.aes = list(size = 5)),colour = guide_legend('Contig true origin',override.aes = list(size = 5))) +
  facet_wrap(~metric_classification) + theme(strip.text.x = element_text(size = 16),strip.text.y = element_text(size = 16))

repeats_prediction_score_plot

```



