#!/bin/bash

# Default values
input_dir=""
output_dir=""
tool=""

# Parse command-line options
while getopts ":i:o:t:" opt; do
  case ${opt} in
    i)
      input_dir=$OPTARG
      ;;
    o)
      output_dir=$OPTARG
      ;;
    t)
      tool=$OPTARG
      ;;
    :)
      echo "Error: Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    \?)
      echo "Error: Invalid option -$OPTARG" >&2
      exit 1
      ;;
  esac
done

# Check if required options are provided
if [ -z "$input_dir" ] || [ -z "$output_dir" ] || [ -z "$tool" ]; then
  echo "Error: Missing required options. Usage: $0 -i <input_dir> -o <output_dir> -t <tool>"
  exit 1
fi

# Validate input directory
if [ ! -d "$input_dir" ]; then
  echo "Error: Input directory '$input_dir' not found."
  exit 1
fi

#1. Get a list of all isolates
isolates=$(ls -p ${input_dir} | grep "/" | sed "s#/##g")

#2. Loop thru each isolate, identify the bins and loop in each bin to run the parse_quast script.
for isolate in ${isolates}
do
if [ "${tool}" != "assembly" ]; then
bins=$(ls -p ${input_dir}/${isolate} | grep "/" | sed "s#/##g")
for bin in ${bins}
do
Rscript parse_quast_output.R ${isolate} ${bin} ${input_dir}/${isolate}/${bin}/contigs_reports/all_alignments*tsv ${output_dir} ${tool}
done
else
Rscript parse_quast_output.R ${isolate} assembly ${input_dir}/${isolate}/contigs_reports/all_alignments*tsv ${output_dir} ${tool}
fi
done

