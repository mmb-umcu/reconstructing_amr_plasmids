This repository contains all code necesary to reproduce the analysis of 
"Classification and reconstruction of plasmids from short-read sequencing 
data for all bacterial species".

Below, find a list of specific codes that need to be run in order to obtain the different outputs.

2023_12_reanalys/src/03_explore_reference_genomes.Rmd
*Supplementary Figure S1 and S4 

2022_04_download_and_wrangle_data/src/SRA_data_analysis.Rmd
*Figure 1

2022_09_general_plascope_model/src/all_refseq_genomes_analysis.Rmd
*Supplementary Figure S2 and S3


2023_12_reanalysis_plasmidCC/src/all_species_results.Rmd
*Table 1 and 2, 
*Figure 2 - 5,
*Supplementary Table S3 - S6
*Supplementary Figures S5 - S15

*Supplementary Table S1 can be obtained by combining the content of three files: 
sra_result_nanopore.csv, sra_result_pacbio.csv and sra_result_illumina.csv.
All files are located in 2022_04_download_and_wrangle_data/data/

*The validation_pipeline directory contains all code needed to run the tool, and to create the Centrifuge DBs.

