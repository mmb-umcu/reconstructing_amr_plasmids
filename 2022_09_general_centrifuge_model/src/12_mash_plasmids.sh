#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh
conda activate mash

##-----------------1. RUN MASH FOR ALL PLASMIDS OF THE SPECIES -------------------------##

#1. Run mash to get the genomic distances between the plasmids
#mkdir for mash results
mkdir -p ../results/benchmark_plasmids_mash

cd ../results/separated_genomes
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids.fasta >> ../benchmark_plasmids_mash/file_paths.txt

cd ../benchmark_plasmids_mash

#create sketches for individual replicons
mash sketch -p 8 -i -l file_paths.txt -o individual_replicons_sketches_21 -k 21 -s 10000

#get mash distances for individual replicons
mash dist -p 8 -i -t individual_replicons_sketches_21.msh -l file_paths.txt > individual_mash_distances_k21.tab

