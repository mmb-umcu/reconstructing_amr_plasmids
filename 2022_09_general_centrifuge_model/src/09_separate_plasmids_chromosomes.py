#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:44:25 2021

@author: jpaganini

Conda environment should be python_codes_mmbioit
"""

import sys
import os
import glob
import fastaparser
import re
from collections import defaultdict

#1. Stablish the working directory
wd=os.path.dirname(os.path.realpath(__file__))
input_directory=sys.argv[1]
output_directory=sys.argv[2]
if not os.path.exists(output_directory):
    os.mkdir(output_directory)

#2. Get a list of genomes
os.chdir(input_directory)
list_strains=glob.glob('*fna')
print(list_strains)

def separate_contigs(genome,input_directory,output_directory):
    strain_name=genome.replace('_genomic.fna','')
    global prediction_parser
    genome_path=input_directory+'/'+genome
    print(genome_path)
    with open(genome_path) as genome_file:
    	genome_parsed = fastaparser.Reader(genome_file, parse_method='quick')
    	for seq in genome_parsed:
            if 'plasmid' in seq.header:    
                with open(output_directory+'/'+strain_name+'_plasmids.fasta', 'a+') as plasmids_fasta:
                    plasmids_fasta.write(seq.header)
                    plasmids_fasta.write('\n')
                    plasmids_fasta.write(seq.sequence)
                    plasmids_fasta.write('\n')
                
            elif 'chromosome' in seq.header:
                 with open(output_directory+'/'+strain_name+'_chromosome.fasta', 'a+') as chromosomes_fasta:
                     chromosomes_fasta.write(seq.header)
                     chromosomes_fasta.write('\n')
                     chromosomes_fasta.write(seq.sequence)
                     chromosomes_fasta.write('\n')


for genome in list_strains:
    separate_contigs(genome,input_directory,output_directory)
