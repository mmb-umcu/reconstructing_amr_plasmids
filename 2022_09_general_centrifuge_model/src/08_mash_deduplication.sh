#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh
conda activate mash

##-----------------1. RUN MASH FOR ALL PLASMIDS OF THE SPECIES -------------------------##

#1. Run mash to get the genomic distances between the plasmids
#mkdir for mash results
mkdir -p ../results/db_mash_distances

cd ../results/genomes_centrifuge_db
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f >> ../db_mash_distances/file_paths.txt

cd ../db_mash_distances

#create sketches for complete genomes
mash sketch -p 8 -l file_paths.txt -o complete_genomes_sketches_29 -k 29 -s 100000

#get mash distances for complete genomes
mash dist -p 8 complete_genomes_sketches_29.msh -l file_paths.txt > complete_genomes_distances_k29.tab

