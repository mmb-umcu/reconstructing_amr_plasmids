#!/bin/bash

conda activate ncbi_download_mmbioit
ncbi-genome-download -l complete --formats fasta -m ../data/all_genomes/all_genomes_metadata.csv --parallel 8 -o ../data/all_genomes bacteria
