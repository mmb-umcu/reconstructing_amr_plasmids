#!/bin/bash

mkdir -p ../results/genomes_centrifuge_db/

#gather list of useful sequences
db_list=$(cat ../results/final_training_list.csv | cut -f 2 -d ,)

#move isolates to a new directory
for reference in $db_list
do
mv ../data/all_genomes/refseq/bacteria/${reference}/*fna ../results/genomes_centrifuge_db
done


