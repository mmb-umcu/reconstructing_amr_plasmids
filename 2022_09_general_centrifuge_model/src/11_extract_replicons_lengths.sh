#!/bin/bash

cd ../results/test_genomes

awk '/^>/{if (l!="") print l; print; l=0; next}{l+=length($0)}END{print l}' *fna |paste - - >> ../benchmark_replicons_length.csv

