#!/bin/bash

cd ../results/separated_genomes/
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../strains_plasmids.csv
