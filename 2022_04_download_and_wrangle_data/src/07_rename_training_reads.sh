#!/bin/bash

#1.move to the directory that contains the reads
cd ../data/sra_files/training_reads
#2. get a list of sra_codes
sra_codes=$(ls *fastq | cut -f 1 -d _ | sort -u)

#2. Move to the results directories, which contains the file that relates the strains_id with the sra_codes
cd ../../../results

#3. Create a series of directories to hold the results
list_species=$(cat short_reads_species_training.csv | cut -f 2 -d , | sed 's/ /_/g' | sort -u)
for specie in $list_species
do
mkdir ../data/sra_files/training_reads/${specie}
done

#4. Rename and move reads
for codes in $sra_codes 
do 
strain=$(grep ${codes} short_reads_species_training.csv | cut -f 1 -d ,)
species=$(grep ${codes} short_reads_species_training.csv | cut -f 2 -d , | sed 's/ /_/g')
mv ../data/sra_files/training_reads/${codes}_1.fastq ../data/sra_files/training_reads/${species}/${strain}_R1.fastq
mv ../data/sra_files/training_reads/${codes}_2.fastq ../data/sra_files/training_reads/${species}/${strain}_R2.fastq
done

#5. get a list of all files
cd ../data/sra_files/training_reads
ls * | grep GCF | sed 's/_R1.fastq//g' | sed 's/_R2.fastq//g' | sort -u > ../../training_isolates_list.csv
