#!/bin/bash

#gunzip ../data/reference_genomes/kpneumoniae/refseq/bacteria/*/*
#gunzip ../data/reference_genomes/paeruginosa/refseq/bacteria/*/*
#gunzip ../data/reference_genomes/saureus/refseq/bacteria/*/*
#gunzip ../data/reference_genomes/salmonella/refseq/bacteria/*/*
#gunzip ../data/reference_genomes/abaumannii/refseq/bacteria/*/*

mkdir ../results/genomes_plascope_db/
mkdir ../results/genomes_plascope_db/paeruginosa
mkdir ../results/genomes_plascope_db/saureus
mkdir ../results/genomes_plascope_db/salmonella
mkdir ../results/genomes_plascope_db/kpneumoniae
#mkdir ../results/genomes_plascope_db/abaumannii

#gather list of useful sequences
kpn_list=$(cat ../results/kpneumoniae_plascope_list.csv)
saureus_list=$(cat ../results/saureus_plascope_list.csv)
paeruginosa_list=$(cat ../results/paeruginosa_plascope_list.csv)
salmonella_list=$(cat ../results/salmonella_plascope_list.csv)
abaumannii_list=$(cat ../results/abaumannii_plascope_list.csv)

#move isolates to a new directory
for genome in $kpn_list
do
cp ../data/reference_genomes/kpneumoniae/refseq/bacteria/${reference}/*fna ../results/genomes_plascope_db/kpneumoniae
done

for genome in $saureus_list
do
reference=$(echo ${genome} | cut -f 1,2 -d _)
cp ../data/reference_genomes/saureus/refseq/bacteria/${reference}/*fna ../results/genomes_plascope_db/saureus
done

for genome in $paeruginosa_list
do
reference=$(echo ${genome} | cut -f 1,2 -d _)
cp ../data/reference_genomes/paeruginosa/refseq/bacteria/${reference}/*fna ../results/genomes_plascope_db/paeruginosa
done

for genome in $salmonella_list
do
reference=$(echo ${genome} | cut -f 1,2 -d _)
cp ../data/reference_genomes/salmonella/refseq/bacteria/${reference}/*fna ../results/genomes_plascope_db/salmonella
done

#for genome in $abaumannii_list
#do
#reference=$(echo ${genome} | cut -f 1,2 -d _)
#cp ../data/reference_genomes/abaumannii/refseq/bacteria/${reference}/*fna ../results/genomes_plascope_db/abaumannii
#done

