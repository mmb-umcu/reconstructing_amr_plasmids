#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh
conda activate mash

##-----------------1. RUN MASH FOR ALL PLASMIDS OF THE SPECIES -------------------------##

#1. Run mash to get the genomic distances between the plasmids
#mkdir for mash results
mkdir -p ../results/plasmid_mash_distances

cd ../results/separated_genomes/abaumannii
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../abaumannii_paths.txt

cd ../senterica
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../senterica_paths.txt

cd ../saureus
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../saureus_paths.txt

cd ../kpneumoniae
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../kpneumoniae_paths.txt

cd ../paeruginosa
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../paeruginosa_paths.txt

cd ../efaecium
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../efaecium_paths.txt

cd ../efaecalis
#Create a txt file that contains a list of paths to the files.
find $(pwd) -type f | grep plasmids >> ../efaecalis_paths.txt

cd ..

#create sketches for complete bins
mash sketch -p 8 -i -l abaumannii_paths.txt -o abaumannii_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l paeruginosa_paths.txt -o paeruginosa_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l senterica_paths.txt -o senterica_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l saureus_paths.txt -o saureus_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l kpneumoniae_paths.txt -o kpneumoniae_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l efaecalis_paths.txt -o efaecalis_sketches_21 -k 21 -s 10000
mash sketch -p 8 -i -l efaecium_paths.txt -o efaecium_sketches_21 -k 21 -s 10000

#get mash 
#in matrix format
mash dist -p 8 -i -t abaumannii_sketches_21.msh -l abaumannii_paths.txt > ../plasmid_mash_distances/abaumannii_distances_21.tab
mash dist -p 8 -i -t paeruginosa_sketches_21.msh -l paeruginosa_paths.txt > ../plasmid_mash_distances/paeruginosa_distances_21.tab
mash dist -p 8 -i -t senterica_sketches_21.msh -l senterica_paths.txt > ../plasmid_mash_distances/senterica_distances_21.tab
mash dist -p 8 -i -t saureus_sketches_21.msh -l saureus_paths.txt > ../plasmid_mash_distances/saureus_distances_21.tab
mash dist -p 8 -i -t kpneumoniae_sketches_21.msh -l kpneumoniae_paths.txt > ../plasmid_mash_distances/kpneumoniae_distances_21.tab
mash dist -p 8 -i -t efaecalis_sketches_21.msh -l efaecalis_paths.txt > ../plasmid_mash_distances/efaecalis_distances_21.tab
mash dist -p 8 -i -t efaecium_sketches_21.msh -l efaecium_paths.txt > ../plasmid_mash_distances/efaecium_distances_21.tab

