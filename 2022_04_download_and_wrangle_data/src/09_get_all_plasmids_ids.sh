#!/bin/bash

cd ../results/separated_genomes/efaecium
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../efaecium_all_plasmids.csv

cd ../efaecalis
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../efaecalis_all_plasmids.csv

cd ../kpneumoniae
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../kpneumoniae_all_plasmids.csv

cd ../abaumannii
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../abaumannii_all_plasmids.csv

cd ../paeruginosa
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../paeruginosa_all_plasmids.csv

cd ../senterica
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../senterica_all_plasmids.csv

cd ../saureus
grep '>' *plasmids* | cut -f 1 -d ' ' | sed "s/_plasmids.fasta:>/,/g" >> ../../saureus_all_plasmids.csv
