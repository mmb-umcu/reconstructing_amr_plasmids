#!/bin/bash

conda activate ncbi_download_mmbioit

#Illumina data
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform illumina"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format summary > ../data/sra_result_illumina.csv.csv
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform illumina"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format runinfo > ../data/SraRunInfo_Illumina.csv

#Nanopore data
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform Oxford Nanopore"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format summary > ../data/sra_result_nanopore.csv
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform Oxford Nanopore"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format runinfo > ../data/SraRunInfo_nanopore.csv


#PacBio data
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform PacBio SMRT"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format summary > ../data/sra_result_pacbio.csv
esearch -db sra -q '("Bacteria"[Organism] OR "Bacteria Latreille et al. 1825"[Organism]) AND "platform PacBio SMRT"[Properties] AND (cluster_public[prop] AND "biomol dna"[Properties] AND "strategy wgs"[Properties])' | efetch -format runinfo > ../data/SraRunInfo_PacBio.csv

