#!/bin/bash

#1- Get the list of the biosamples Id to check if we have the reads that will later be used to run the alignments

biosamples=$(cat ../results/all_biosamples.csv)

training_biosamples=$(cat ../results/all_biosamples.csv)

#2. Now, we will get the reads identifyiers

for biosample in $biosamples
do
sra_accession=$(esearch -db biosample -query "${biosample}" | elink -target sra | efetch -format runinfo  | grep 'Illumina\|ILLUMINA' | cut -f 1 -d ,)
echo ${biosample},${sra_accession} >> ../results/sra_accessions_list.csv
done


for biosample in $training_biosamples
do
sra_accession=$(esearch -db biosample -query "${biosample}" | elink -target sra | efetch -format runinfo  | grep 'Illumina\|ILLUMINA' | cut -f 1 -d ,)
echo ${biosample},${sra_accession} >> ../results/sra_accessions_list_training_samples.csv
done
