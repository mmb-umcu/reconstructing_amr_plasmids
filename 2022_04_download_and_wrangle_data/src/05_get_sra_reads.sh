#!/bin/bash

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate sra_tools_mmbioit

#1. Get a list of sra_ids 
sra_accessions=$(cat ../results/sra_accessions_list.csv | cut -f 2 -d ,)
sra_accessions_training=$(cat ../results/sra_accessions_list_training_samples.csv | cut -f 2 -d ,)

#2. Make a directory for holding the results
mkdir ../data/sra_files
mkdir ../data/sra_files_training


#3. Make a loop to download the srr files
for reads in $sra_accessions
do
fasterq-dump --split-files ${reads} -O ../data/sra_files   
done


#3. Make a loop to download the srr files
for reads in $sra_accessions_training
do
fasterq-dump --split-files ${reads} -O ../data/sra_files_training   
done
