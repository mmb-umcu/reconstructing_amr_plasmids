#!/bin/bash

conda activate ncbi_download_mmbioit
mkdir ../data/reference_genomes

ncbi-genome-download --genera "Klebsiella pneumoniae" -l complete --formats fasta -m ../data/reference_genomes/kpneumoniae_metadata.csv --parallel 4 -o ../data/reference_genomes/kpneumoniae bacteria

ncbi-genome-download --genera "Salmonella enterica" -l complete --formats fasta -m ../data/reference_genomes/senterica_metadata.csv --parallel 4 -o ../data/reference_genomes/senterica bacteria

ncbi-genome-download --genera "Staphylococcus aureus" -l complete --formats fasta -m ../data/reference_genomes/saureus_metadata.csv --parallel 8 -o ../data/reference_genomes/saureus bacteria

ncbi-genome-download --genera "Acinetobacter baumannii" -l complete --formats fasta -m ../data/reference_genomes/abaumannii_metadata.csv --parallel 8 -o ../data/reference_genomes/abaumannii bacteria

ncbi-genome-download --genera "Pseudomonas aeruginosa" -l complete --formats fasta -m ../data/reference_genomes/paeruginosa_metadata.csv --parallel 8 -o ../data/reference_genomes/paeruginosa bacteria

ncbi-genome-download --genera "Enterococcus faecalis" -l complete --formats fasta -m ../data/reference_genomes/efaecalis_metadata.csv --parallel 8 -o ../data/reference_genomes/efaecalis bacteria

ncbi-genome-download --genera "Enterococcus faecium" -l complete --formats fasta -m ../data/reference_genomes/efaecium_metadata.csv --parallel 8 -o ../data/reference_genomes/efaecium bacteria

ncbi-genome-download --genera "Streptococcus pneumoniae" -l complete --formats fasta -m ../data/reference_genomes/spneumoniae_metadata.csv --parallel 8 -o ../data/reference_genomes/spneumoniae/ bacteria

ncbi-genome-download --genera "Streptococcus pyogenes" -l complete --formats fasta -m ../data/reference_genomes/spyogenes_metadata.csv --parallel 8 -o ../data/reference_genomes/spyogenes/ bacteria

ncbi-genome-download --genera "Streptococcus agalactiae" -l complete --formats fasta -m ../data/reference_genomes/sagalactiae_metadata.csv --parallel 8 -o ../data/reference_genomes/sagalactiae/ bacteria

ncbi-genome-download --genera "Campylobacter jejuni" -l complete --formats fasta -m ../data/reference_genomes/cjejuni_metadata.csv --parallel 8 -o ../data/reference_genomes/cjejuni bacteria

ncbi-genome-download --genera "Listeria monocytogenes" -l complete --formats fasta -m ../data/reference_genomes/lmonocytogenes_metadata.csv --parallel 8 -o ../data/reference_genomes/lmonocytogenes bacteria

ncbi-genome-download --genera "Mycobacterium tuberculosis" -l complete --formats fasta -m ../data/reference_genomes/mtuberculosis_metadata.csv --parallel 8 -o ../data/reference_genomes/mtuberculosis bacteria

ncbi-genome-download --genera "Neisseria meningitidis" -l complete --formats fasta -m ../data/reference_genomes/nmeningitidis_metadata.csv --parallel 8 -o ../data/reference_genomes/meningitidis bacteria

ncbi-genome-download --genera "Clostridioides difficile" -l complete --formats fasta -m ../data/reference_genomes/cdifficile_metadata.csv --parallel 8 -o ../data/reference_genomes/cdifficile bacteria

#Gather all metadata into a single file
cat ../data/referece_genomes/*metadata.csv >> ../data/reference_genomes/all_metadata.csv

#unzip files
gunzip ../data/reference_genomes/kpneumoniae/refseq/bacteria/*/*
gunzip ../data/reference_genomes/paeruginosa/refseq/bacteria/*/*
gunzip ../data/reference_genomes/saureus/refseq/bacteria/*/*
gunzip ../data/reference_genomes/salmonella/refseq/bacteria/*/*
gunzip ../data/reference_genomes/efaecalis/refseq/bacteria/*/*
gunzip ../data/reference_genomes/efaecium/refseq/bacteria/*/*
gunzip ../data/reference_genomes/abaumannii/refseq/bacteria/*/*

